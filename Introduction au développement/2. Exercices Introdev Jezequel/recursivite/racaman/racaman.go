package racaman

/*
La suite de Racaman est définie par a(1) = 1, puis pour n > 1 par :
- a(n-1) - n si ce nombre est strictement supérieur à 0 et n'a encore jamais été
vu dans la suite
- a(n-1) + n sinon

La fonction racaman doit calculer a(n) pour n supérieur ou égal à 1.

# Entrée
- n : le numéro du terme de la suite à calculer

# Sortie
- an : la valeur du terme de la suite calculé (si ce terme n'est pas défini, on
retournera -1)

# Exemple
racaman(4) = 2
*/
func dedans(liste []int, val int) bool{
    for _, v:= range liste{
        if v == val{
            return true
        }
    }
    return false
}


func racamanListe(n int) []int{
    if n < 1{
        return []int{-1}
    }
    if n == 1{
        return []int{1}
    }
    var prec []int = racamanListe(n-1)
    var moinsN int = prec[len(prec)-1] - n
    if moinsN > 0 && !dedans(prec, moinsN){
        return append(prec, moinsN)
    }
    return append(prec, prec[len(prec)-1] + n)
}



func racaman(n int) (an int) {
	var res []int = racamanListe(n)
    return res[len(res)-1]
}
