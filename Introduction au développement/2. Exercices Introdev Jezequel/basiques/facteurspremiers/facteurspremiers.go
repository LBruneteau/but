package facteurspremiers

/*
La fonction facteursPremiers doit retourner un tableau contenant la liste de tous
les facteurs premiers d'un entier n, doublons compris

# Entrée
- n : un nombre entier positif

# Sortie
- facteurs : un tableau contenant tous les facteurs premiers de n, si n vaut 0 il
faut retourner un tableau à zéro éléments.

# Exemple
premiers(24) = [2 2 2 3] (l'ordre n'a pas d'importance)
*/
func estPremier(n uint) bool {
	var p []uint = prems(n)
	for i := 0; i < len(p)-1; i++ {
		if n%p[i] == 0 {
			return false
		}
	}
	return true
}

func prems(n uint) []uint {
	var res []uint
	var ok bool
	for i := uint(2); i <= n; i++ {
		ok = true
		for j := 0; j < len(res); j++ {
			if i%res[j] == 0 {
				ok = false
				break
			}
		}
		if ok {
			res = append(res, i)
		}
	}
	return res
}

func facteursPremiers(n uint) (facteurs []uint) {
	if n <= uint(1) {
		return make([]uint, 0)
	}
	var res []uint
	var facts []uint = prems(n)
	for !estPremier(n) {
		for i := 0; i < len(facts); i++ {
			if n%facts[i] == 0 {
				res = append(res, facts[i])
				n /= facts[i]
				break
			}
		}
	}
	return append(res, n)
}
