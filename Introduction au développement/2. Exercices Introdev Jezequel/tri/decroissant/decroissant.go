package decroissant

/*
La fonction décroissant doit trier un tableau d'entiers du plus grand
au plus petit.

# Entrée
- tab : le tableau à trier
*/

func decroissant(tablo []int) {
	for i := 1; i < len(tablo); i++ {
		var j int = i
		for j > 0 && tablo[j] > tablo[j-1] {
			tablo[j], tablo[j-1] = tablo[j-1], tablo[j]
			j -= 1
		}
	}
}
