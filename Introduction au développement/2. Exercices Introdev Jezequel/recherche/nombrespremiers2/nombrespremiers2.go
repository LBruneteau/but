package nombrespremiers2

/*
La fonction selectionPremiers filtre le contenu d'un tableau d'entiers pour ne garder que ceux qui sont des nombres premiers, en éliminant les doublons.

# Entrée
- t : un tableau d'entiers

# Sortie
- p : un tableau contenant tous les nombres premiers de t, sans doublons.
Si t est vide, p doit être identique à t.

# Exemple
selectionPremiers([]int{1, 2, 2, 3, 4, 5}) = [2 3 5] (l'ordre n'a pas d'importance)
*/

func estPremier(n int) bool {
	if n <= 1 {
		return false
	}
	for i := 2; i < n; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

func dans(n int, tablo []int) bool {
	for _, el := range tablo {
		if el == n {
			return true
		}
	}
	return false
}

func selectionPremiers(t []int) (p []int) {
	if t == nil {
		return nil
	}
	p = []int{}
	for _, val := range t {
		if estPremier(val) && !dans(val, p) {
			p = append(p, val)
		}
	}
	return p
}
