def nb_vers_case(case: int, taille=8) -> str:
    return 'abcdefghijklmnopqrstuvwxyz'[case // taille] + str((case % taille) + 1)

def est_valide(echiquier, taille=8):
    for i, dame in enumerate(echiquier[:-1]):
        ligne, colonne = divmod(dame, taille)
        for dame_2 in echiquier[i+1:]:
            ligne_2, colonne_2 = divmod(dame_2, taille)
            if ligne == ligne_2:
                return False
            if colonne == colonne_2:
                return False
            if (ligne + colonne) == (ligne_2 + colonne_2) or (ligne - colonne) == (ligne_2 - colonne_2):
                return False
    return True

def dames(echiquier=[], taille=8):
    '''Premier solve trouvé
    '''
    if len(echiquier) == taille:
        return echiquier, est_valide(echiquier, taille)
    for i in range(taille):
        echiquier2 = echiquier[:] + [taille*len(echiquier)+i]
        if est_valide(echiquier2, taille):
            d = dames(echiquier2, taille)
            if d[1]:
                return d
    return echiquier, False

def toutes_solutions(echiquier=[], taille=8, solves=set()):
    if len(echiquier) == taille:
        if est_valide(echiquier, taille):
            solves.add(" ".join([nb_vers_case(d, taille) for d in echiquier]))
        return solves
    for i in range(taille):
        echiquier2 = echiquier[:] + [taille*len(echiquier)+i]
        if est_valide(echiquier2, taille):
            toutes_solutions(echiquier2, taille, solves)
    return solves

if __name__ == '__main__':
    print([nb_vers_case(d) for d in dames()[0]], end='\n'+'-'*100+'\n') # ['a1', 'b5', 'c8', 'd6', 'e3', 'f7', 'g2', 'h4']
    print([nb_vers_case(d, 4) for d in dames(taille=4)[0]], end='\n'+'-'*100+'\n') # ['a2', 'b4', 'c1', 'd3']
    print(toutes_solutions(taille=4), end='\n'+'-'*100+'\n')
    print(toutes_solutions(taille=5), end='\n'+'-'*100+'\n')
    print(toutes_solutions(taille=8), end='\n'+'-'*100+'\n')
