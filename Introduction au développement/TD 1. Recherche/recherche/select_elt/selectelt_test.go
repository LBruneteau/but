package select_elt

import (
	"testing"
)

func TestNull(t *testing.T) {
	res := SelectElt(nil, 69)
	if len(res) > 0 {
		t.Fail()
	}
}

func TestVide(t *testing.T) {
	res := SelectElt(nil, 69)
	if len(res) > 0 {
		t.Fail()
	}
}

func TestDedans(t *testing.T) {
	val := SelectElt([]int{9, 2, 8, 3, 6, 5}, 6)
	if val[0] != 2 || val[1] != 3 || val[2] != 6 || val[3] != 5 {
		t.Fail()
	}
}

func TestPasDedans(t *testing.T) {
	res := SelectElt([]int{2, 2, 3, 4, 5}, 1)
	if len(res) > 0 {
		t.Fail()
	}
}
