package select_elt

func SelectElt(tablo []int, valeur int) []int {
	var res []int
	for _, val := range tablo {
		if val <= valeur {
			res = append(res, val)
		}
	}
	return res
}
