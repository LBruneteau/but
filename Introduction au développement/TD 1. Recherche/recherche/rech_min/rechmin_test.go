package rech_min

import (
	"testing"
)

func TestNil(t *testing.T) {
	_, index, err := RechMin(nil)
	if index >= 0 || err == nil {
		t.Fail()
	}
}

func TestVide(t *testing.T) {
	_, index, err := RechMin([]int{})
	if index >= 0 || err == nil {
		t.Fail()
	}
}

func TestNormal(t *testing.T) {
	val, index, err := RechMin([]int{5, 6, 7, 1, 3, 2, 4})
	if val != 1 || index != 3 || err != nil {
		t.Fail()
	}
}

func TestDoublon(t *testing.T) {
	val, _, err := RechMin([]int{5, 1, 7, 1, 3, 1, 4})
	if val != 1 || err != nil {
		t.Fail()
	}
}
