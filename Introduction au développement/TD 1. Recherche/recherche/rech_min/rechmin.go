package rech_min

import (
	"errors"
)

var errPasTablo error = errors.New("tu pues")

func RechMin(tablo []int) (int, int, error) {
	if len(tablo) == 0 {
		return 0, -1, errPasTablo
	}
	var val_min int = tablo[0]
	var index_min int
	for index, valeur := range tablo[1:] {
		if valeur < val_min {
			val_min = valeur
			index_min = index + 1
		}
	}
	return val_min, index_min, nil
}
