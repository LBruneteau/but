package rech_elt_dicho

import (
	"testing"
)

func TestNil(t *testing.T) {
	_, err := RechEltDicho(nil, 420)
	if err != errPasDedans {
		t.Fail()
	}
}

func TestVide(t *testing.T) {
	_, err := RechEltDicho([]int{}, 69)
	if err != errPasDedans {
		t.Fail()
	}
}

func TestPasDedans(t *testing.T) {
	_, err := RechEltDicho([]int{1, 2, 3, 4, 5}, 69)
	if err != errPasDedans {
		t.Fail()
	}
}

func TestDedans(t *testing.T) {
	val, err := RechEltDicho([]int{1, 2, 4, 8, 55, 69, 420}, 69)
	t.Log(val)
	if val != 5 || err != nil {
		t.Fail()
	}
}

func TestDebut(t *testing.T) {
	val, err := RechEltDicho([]int{1, 2, 4, 8, 55, 69, 420}, 1)
	if val != 0 || err != nil {
		t.Fail()
	}
}

func TestFin(t *testing.T) {
	val, err := RechEltDicho([]int{1, 2, 4, 8, 55, 69, 420}, 420)
	t.Log(val)
	if val != 6 || err != nil {
		t.Fail()
	}
}
