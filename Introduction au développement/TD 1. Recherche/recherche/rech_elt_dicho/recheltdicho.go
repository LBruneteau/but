package rech_elt_dicho

import (
	"errors"
)

var errPasDedans error = errors.New("l'élément n'est pas dans la liste")

func RechEltDicho(tablo []int, val int) (int, error) {
	if len(tablo) == 0 {
		return -1, errPasDedans
	}
	if len(tablo) == 1 {
		if val == tablo[0] {
			return 0, nil
		}
		return -1, errPasDedans
	}
	var milieu int = len(tablo) / 2
	if val > tablo[milieu] {
		suivant, err := RechEltDicho(tablo[milieu+1:], val)
		return suivant + milieu + 1, err
	}
	if val < tablo[milieu] {
		return RechEltDicho(tablo[:milieu], val)
	}
	return milieu, nil

}
