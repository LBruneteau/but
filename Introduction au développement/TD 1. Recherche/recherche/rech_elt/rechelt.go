package rech_elt

import (
	"errors"
)

var errFormatTablo error = errors.New("t'es pas le couteau le plus affuté du tiroir")

func RechElt(tablo []int, val int) (b bool, pos int, err error) {
	if tablo == nil {
		return false, -1, errFormatTablo
	}
	for index, element := range tablo {
		if element == val {
			return true, index, nil
		}
	}
	return false, -1, nil
}
