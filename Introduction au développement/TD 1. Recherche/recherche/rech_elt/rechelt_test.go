package rech_elt

import "testing"

func TestNil(t *testing.T) {
	boobool, ind, err := RechElt(nil, 69)
	if err != errFormatTablo || boobool || ind >= 0 {
		t.Fail()
	}
}

func TestVide(t *testing.T) {
	res, ind, err := RechElt([]int{}, 69)
	if res || err != nil || ind >= 0 {
		t.Fail()
	}
}

func TestDedans(t *testing.T) {
	res, index, err := RechElt([]int{1, 2, 69, 42}, 69)
	if !res || index != 2 || err != nil {
		t.Fail()
	}
}

func TestPasDedans(t *testing.T) {
	res, ind, err := RechElt([]int{1, 2, 3, 4, 5, 6}, 7)
	if res || err != nil || ind >= 0 {
		t.Fail()
	}
}
