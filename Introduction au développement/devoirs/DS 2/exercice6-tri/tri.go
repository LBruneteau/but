package tri

/*
La fonction tri doit trier un tableau d'entiers du plus petit au plus grand.
Cette fonction ne doit pas modifier le tableau donné en entrée.

# Entrée
- tinit : un tableau d'entiers qui ne doit pas être modifié.

# Sortie
- tfin : un tableau contenant les mêmes entiers que tinit mais triés du plus
         petit au plus grand.
*/
func fusionner(tablblbl_a, tablblbl_b []int) []int {
	if len(tablblbl_a) == 0 {
		return tablblbl_b
	}
	if len(tablblbl_b) == 0 {
		return append(tablblbl_a)
	}
	var res []int
	if tablblbl_a[0] < tablblbl_b[0] {
		res = []int{tablblbl_a[0]}
		return append(res, fusionner(tablblbl_a[1:], tablblbl_b)...)
	}
	res = []int{tablblbl_b[0]}
	return append(res, fusionner(tablblbl_b[1:], tablblbl_a)...)
}

func tri(tablblbl []int) []int {
	var l int = len(tablblbl)
	if l <= 1 {
		return tablblbl
	}
	return fusionner(tri(tablblbl[:l/2]), tri(tablblbl[l/2:]))
}
