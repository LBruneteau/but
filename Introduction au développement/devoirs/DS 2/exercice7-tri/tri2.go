package tri2

/*
La fonction triabs doit trier un tableau d'entiers de la plus grande valeure
absolue à la plus petite valeure absolue. Cette fonction ne doit pas modifier
le tableau donné en entrée.

# Entrée
- tinit : un tableau d'entiers qui ne doit pas être modifié.

# Sortie
- tfin : un tableau contenant les mêmes entiers que tinit mais triés du plus
         grand (en valeure absolue) au plus petit (en valeure absolue).
*/
func absolu(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func fusionner(tablblbl_a, tablblbl_b []int) []int {
	if len(tablblbl_a) == 0 {
		return tablblbl_b
	}
	if len(tablblbl_b) == 0 {
		return tablblbl_a
	}
	var res []int
	if absolu(tablblbl_a[0]) > absolu(tablblbl_b[0]) { // Changement par rapport au 6 ici
		res = []int{tablblbl_a[0]}
		return append(res, fusionner(tablblbl_a[1:], tablblbl_b)...)
	}
	res = []int{tablblbl_b[0]}
	return append(res, fusionner(tablblbl_b[1:], tablblbl_a)...)
}

func triabs(tablblbl []int) []int {
	var l int = len(tablblbl)
	if l <= 1 {
		return tablblbl // Saucisse
	}
	return fusionner(triabs(tablblbl[:l/2]), triabs(tablblbl[l/2:]))
}
