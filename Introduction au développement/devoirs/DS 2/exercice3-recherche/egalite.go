package egalite

/*
On considère des ensembles de nombres représentés par des tableaux : on considère
que ces tableaux ne contiennent qu'une seule fois chaque nombre (puisqu'ils
représentent des ensembles) et les nombres ne sont pas nécessairement dans
l'ordre dans les tableaux.

On veut savoir si deux ensembles sont égaux ou pas, c'est-a-dire savoir si deux
tableaux contiennent les mêmes nombres ou pas. C'est à cette question que doit
répondre la fonction egalite.

# Entrées
- t1 : un tableau d'entiers (sans doublons) représentant un ensemble
- t2 : un tableau d'entiers (sans doublons) représentant un ensemble

# Sortie
- egaux : un booléen qui vaut true si t1 et t2 représentent le même ensemble et
          qui vaut false sinon
*/
func dansTablo(tablblbl []int, blbl int) bool {
	for _, val := range tablblbl {
		if val == blbl {
			return true
		}
	}
	return false
}

func inclus(a, b []int) bool {
	// retourne true si b est inclus dans a
	for _, val := range a {
		if !dansTablo(b, val) {
			return false
		}
	}
	return true
}

func egalite(tablblbl, tablo []int) (egaux bool) {
	return inclus(tablblbl, tablo) && inclus(tablo, tablblbl)
}
