# Les bases du langage Go

## Partie 1 : variables, tableaux, boucles, conditionnelles,fonctions

**Initiation au développement**
**BUT informatique, première année**

*Ce TP a pour but de vous présenter les bases du langage Go. C’est ce langage qui sera utilisé pour réaliser tous les exercices de ce cours d’initiation au développement. Il n’y a pas de prérequis particuliers en dehors des notions de conditionnelle, de boucle et de variable, qui ont été étudiées au lycée.*

Si vous souhaitez avoir un autre point de vue sur l’apprentissage du langage Go, vous pouvez consulter le tutoriel officiel [*A Tour of Go*](https://tour.golang.org/) ou le site [*Go by Example*](https://gobyexample.com/) en faisant toute fois attention au fait que ces sites sont plutôt faits pour les personnes ayant déjà une bonne expérience de la programmation.

## 1. Un premier programme

Le programme 1 est un code Go très simple. Avant d’expliquer les différents éléments qui le constituent nous allons voir comment l’exécuter, c’est-à-dire comment le faire fonctionner sur un ordinateur.

### Écrire un programme

fichier *hello.go*

```go
package main // Tous les programmes commencent par cette ligne
import "fmt" // Importe une bibliothèque nommée fmt affichant du contenu dans le terminal

func main(){ // Crée une fonction nommée main qui s'éxecutera avec le programme
    fmt.Println("Hello World !") // Affiche "Hello World !" dans le terminal
} // Fin de la fonction
```

### Éxecuter le programme

1. Se placer dans le répertoire du fichier dans le terminal.
2. Lancer la commande `go run fichier.go`

Le résultat sera affiché dans le terminal.

## 2. Les variables

Syntaxe : `var nom type = valeur`

```go
package main
import "fmt"
func main(){
    var n int
    fmt.Println(n) // Affiche la valeur de n -> 0 par défaut

    var b bool
    fmt.Println(b) // Valeur de b -> false par défaut

    var s string
    fmt.Println(s) // Valeur de s -> "" (chaîne de caractères vide) par défaut
}
```

+ `int` représente un nombre entier
+ `bool` représente un booléen (Vrai ou faux)
+ `string` représente une chaîne de caractères
+ `float64` représente un nombre à virgule flottante (décimal)

Les variables ne peuvent pas changer de type.

**Attention** : En Go, si on déclare une variable on doit obligatoirement l'utiliser ensuite

### Réaffectation

```go
package main
import "fmt"

func main(){
    var n int = 1
    fmt.Println(n) // 1
    n = 2
    fmt.Println(n) // 2
}
```
