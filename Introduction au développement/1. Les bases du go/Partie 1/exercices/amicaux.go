package main

import "fmt"

func f(x int) int {
	var somme int
	for i := 1; i < x; i++ {
		if x%i == 0 {
			somme += 1
		}
	}
	return somme
}

func amicaux(x int, y int) bool {
	return f(x) == y && f(y) == x
}

func main() {
	fmt.Println(amicaux(220, 284))
}
