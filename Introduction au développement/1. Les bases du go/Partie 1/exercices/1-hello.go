package main

import "fmt"

// Objectif : Afficher Hello World depuis une variable
func main() {
	var hello string = "Hello World!"
	fmt.Println(hello)
}
