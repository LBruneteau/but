package main

import "log"

func replie(t []int, f func(int, int) int) int {
	var somme int
	for _, v := range t {
		somme = f(v, somme)
	}
	return somme
}

func main() {
	var t []int = []int{1, 3, 2, 0, -3, 27, 5}
	var f func(int, int) int = func(a, b int) int { return a + b }
	log.Print(replie(t, f))
}
