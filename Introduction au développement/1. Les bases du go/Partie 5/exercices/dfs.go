package main

import "log"

func add(a, b int) int { return a + b }

func buildSpecificAdder(n int) func(int) int {
	return func(a int) int { return add(a, n) }
}

func main() {
	var f = buildSpecificAdder(10)
	log.Print(f(5))
}
