package main

import "fmt"

func main() {
	fmt.Println(multiples(50))
}

func multiples(n int) []int {
	var tablo []int = make([]int, n)
	for i := 0; i < n; i++ {
		tablo[i] = 7 * i
	}
	return tablo
}
