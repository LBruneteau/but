package main

import "fmt"

func main() {
	fmt.Println(tables(10, 10))
}

func tables(n int, m int) [][]int {
	var tablo [][]int = make([][]int, n)
	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			tablo[i] = append(tablo[i], i*j)
		}
	}
	return tablo
}
