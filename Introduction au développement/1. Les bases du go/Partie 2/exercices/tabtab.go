package main

import "fmt"

func main() {
	var mat [][]int = [][]int{
		[]int{1, 2, 3},
		[]int{4, 5},
		[]int{6, 7, 8, 9, 10},
	}

	fmt.Println("tab =", mat)
	fmt.Println("tab[0] =", mat[0])
	fmt.Println("tab[0][1] =", mat[0][1])
}
