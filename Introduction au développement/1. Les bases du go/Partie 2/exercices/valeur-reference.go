package main
import "fmt"

func f(x int, y int) {
	x = x + y
}
// valeur et pas référence
func main() {
	var n int = 1
	var m int = 2
	f(n, m)
	fmt.Println(n)
}
