package main

import (
	"flag"
	"fmt"
)

func main() {
	var x int
	var y int
	var z int

	flag.IntVar(&x, "x", 1, "Valeur de x")
	flag.IntVar(&y, "y", 1, "Valeur de y")
	flag.IntVar(&z, "z", 1, "Valeur de z")
	flag.Parse()

	var tablo []int
	for i := 0; i <= y; i++ {
		if x*i%z != 0 {
			tablo = append(tablo, x * i)
		}
	}
	fmt.Println(tablo)
}
