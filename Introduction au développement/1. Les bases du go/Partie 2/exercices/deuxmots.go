package main
import "fmt"


func indexespace(mot string) int{
    for i:=0 ; i < len(mot); i++{
      if mot[i] == ' '{
        return i
      }
    }
    return -1
}

func separmo(mot string) (string, string){
  var i_espace = indexespace(mot)
  return mot[:i_espace], mot[i_espace+1:]
}

func main(){
  var a string
  var b string
  var c string = "caca boudin"
  a, b = separmo(c)
  fmt.Println(a)
  fmt.Println(b)
}
