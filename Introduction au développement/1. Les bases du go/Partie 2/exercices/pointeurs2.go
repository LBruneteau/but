package main

import "fmt"

func main() {
	var n int = 5
	var a *int = &n
	var b **int = &a
	var c ***int = &b
	fmt.Println(n, *a, **b, ***c) // 5 5 5 5

	***c = 7
	fmt.Println(n, *a, **b, ***c) // 7 7 7 7
}
