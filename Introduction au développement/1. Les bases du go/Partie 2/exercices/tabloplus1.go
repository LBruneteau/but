package main

import "fmt"
import "math/rand"
import "time"

func tablorandom(longueur int, minimum int, maximum int) []int{
	var tablo []int
	rand.Seed(time.Now().UnixNano())
	for i:=0; i < longueur; i++{
		tablo = append(tablo, rand.Intn(maximum - minimum + 1) + minimum)
	}
	return tablo
}

func main(){
	var tablo []int = tablorandom(20, 5, 10)
	fmt.Println(tablo)
	for i:= 0; i < len(tablo) ; i++{
		tablo[i] += 1
	}
	fmt.Println(tablo)
}