package main

import "fmt"

// Au début n vaut 0 et m vaut 10
// Après l'itération 0 n vaut 1 et m vaut 8
// Après l'itération 1 n vaut 8 et m vaut 1
// Après l'itération 2 n vaut 9 et m vaut -1
// Après l'itération 3 n vaut -1 et m vaut 9
// Après l'itération 4 n vaut 0 et m vaut 7
// Après l'itération 5 n vaut 7 et m vaut 0
// Après l'itération 6 n vaut 8 et m vaut -2
// Après l'itération 7 n vaut -2 et m vaut 8
// Après l'itération 8 n vaut -1 et m vaut 6
// Après l'itération 9 n vaut 6 et m vaut -1
// À la fin n vaut 6 et m vaut -1

func f(x *int, y *int, b bool) {
	if b {
		*x += 1
		*y -= 2
	} else{
		*x, *y = *y, *x 
	}
}

func main() {
	var n int = 0
	var m int = 10
	fmt.Println("Au début n vaut", n, "et m vaut", m)
	for i := 0; i < 10; i++ {
		f(&n, &m, i%2 == 0)
		fmt.Println("Après l'itération", i, "n vaut", n, "et m vaut", m)
	}
	fmt.Println("À la fin n vaut", n, "et m vaut", m)
}
