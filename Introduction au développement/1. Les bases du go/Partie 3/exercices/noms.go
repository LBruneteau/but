package main

import (
	"bufio"
	"log"
	"os"
	"strings"
	"fmt"
)

func in(s string, t []string) bool{
	for i:=0 ; i<len(t) ; i++{
		if t[i] == s{
			return true
		}
	}
	return false
}

func main(){
	var chemin string = "notes.csv"
	var fichiai *os.File
	var oof error
	var ecriture string
	var tablo []string
	var res []string
	var nom string
	var debut bool = true
	fichiai, oof = os.Open(chemin)
	if oof != nil{
		log.Fatal(oof)
	}
	var scanner *bufio.Scanner = bufio.NewScanner(fichiai)
	for scanner.Scan(){
		ecriture = scanner.Text()
		if debut {
			debut = false
			continue
		}
		tablo = strings.Split(ecriture, ",")
		nom = tablo[0]
		if !in(nom, res){
			res = append(res, nom)
		}
	}
	fmt.Println(res)
	
}