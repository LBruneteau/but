package main
import (
	"fmt"
	"os"
	"log"
	"flag"
)

func main(){
	var n int
	flag.IntVar(&n, "n", 1, "Valeur de n")
	flag.Parse()
	var fichiai *os.File
	var oof error
	fichiai, oof = os.Create(fmt.Sprintf("Table de %d.txt", n))
	for i:=0 ; i<= 10; i++{
		_, oof = fmt.Fprintln(fichiai, fmt.Sprintf("%d x %d = %d", i, n, i*n))
		if oof != nil{
			log.Fatal(oof)
		}
	}
}