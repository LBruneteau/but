package main

import (
	"bufio"
	"log"
	"os"
	"strings"
	"fmt"
	"strconv"
	"flag"
)

func main(){
	var chemin string = "notes.csv"
	var fichiai *os.File
	var oof error
	var ecriture string
	var tablo []string
	var nom string
	var note int
	var nb int
	var somme float64
	flag.StringVar(&nom, "nom", "", "L'élève dont la moyenne est calculée")
	flag.Parse()
	fichiai, oof = os.Open(chemin)
	if oof != nil{
		log.Fatal(oof)
	}
	var scanner *bufio.Scanner = bufio.NewScanner(fichiai)
	for scanner.Scan(){
		ecriture = scanner.Text()
		tablo = strings.Split(ecriture, ",")
		if tablo[0] == nom{
			note, _ = strconv.Atoi(tablo[3])
			somme +=float64(note)
			nb += 1
		}
	}
	fmt.Println(somme / float64(nb))	
}