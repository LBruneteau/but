package main

import (
	"fmt"
	"math"
)

// Interface
type Forme interface {
	Perimetre() float64
	Aire() float64
}

func PlusGrandPerimetre(f1, f2 Forme) bool {
	return f1.Perimetre() > f2.Perimetre()
}

func PlusGrandeAire(f1, f2 Forme) bool {
	return f1.Aire() > f2.Aire()
}

// Implantation pour les carrés
type Carre struct {
	Cote float64
}

func (c Carre) Perimetre() float64 {
	return 4 * c.Cote
}

func (c Carre) Aire() float64 {
	return c.Cote * c.Cote
}

// Implantation pour les cercles
type Cercle struct {
	Rayon float64
}

func (c Cercle) Perimetre() float64 {
	return 2 * math.Pi * c.Rayon
}

func (c Cercle) Aire() float64 {
	return c.Rayon * math.Pi * c.Rayon
}

type Rectangle struct {
	longueur float64
	largeur  float64
}

func (r Rectangle) Perimetre() float64 {
	return 2 * (r.longueur + r.largeur)
}

func (r Rectangle) Aire() float64 {
	return r.longueur * r.largeur
}

func main() {
	var ca Carre = Carre{Cote: 5}
	var ce Cercle = Cercle{Rayon: 2.5}
	fmt.Println(PlusGrandPerimetre(ca, ce))
}
