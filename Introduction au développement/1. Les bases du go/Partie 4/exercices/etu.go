package main

type Etudiant struct {
	nom    string
	prenom string
	notes  []float64
}

func (etu Etudiant) Moyenne() float64 {
	var moy float64
	if len(etu.notes) == 0 {
		return float64(0)
	}
	for _, n := range etu.notes {
		moy += n
	}
	return moy / float64(len(etu.notes))
}

func (etu *Etudiant) AjouteNote(note float64) {
	etu.notes = append(etu.notes, note)
}

type Promo struct {
	nom       string
	etudiants []Etudiant
}

func (promo *Promo) TriEtudiants() {
	for i := 1; i < len(promo.etudiants); i++ {
		var j int = i
		for j > 0 && promo.etudiants[j].Moyenne() < promo.etudiants[j-1].Moyenne() {
			promo.etudiants[j], promo.etudiants[j-1] = promo.etudiants[j-1], promo.etudiants[j]
			j -= 1
		}
	}
}
