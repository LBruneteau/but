package tris

func InsertPlace(pwinter *[]int) {
	var tablo []int = *pwinter
	for i := 1; i < len(tablo); i++ {
		var j int = i
		for j > 0 && tablo[j] < tablo[j-1] {
			tablo[j], tablo[j-1] = tablo[j-1], tablo[j]
			j -= 1
		}

	}
}

func Insert(tablo []int) []int {
	var copie []int
	copy(copie, tablo)
	InsertPlace(&copie)
	return copie
}

func fusionner(a []int, b []int) []int {
	if len(a) == 0 {
		return b
	}
	if len(b) == 0 {
		return a
	}
	if a[0] < b[0] {
		return append(a[:1], fusionner(a[1:], b)...)
	}
	return append(b[:1], fusionner(a, b[1:])...)
}

func Fusion(tablo []int) []int {
	var l int = len(tablo)
	if l <= 1 {
		return tablo
	}
	return fusionner(tablo[:l/2], tablo[l/2:])
}

func partitionner(tablo []int) ([]int, []int) {
	var a []int
	var b []int
	for _, val := range tablo[1:] {
		if val < tablo[0] {
			a = append(a, val)
		} else {
			b = append(b, val)
		}
	}
	return a, b
}

func Rapide(tablo []int) []int {
	if len(tablo) <= 1 {
		return tablo
	}
	a, b := partitionner(tablo)
	return append(append(a, tablo[0]), b...)
}
