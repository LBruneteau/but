package tris

import "testing"

func tabloEgal(a []int, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, ela := range a {
		if ela != b[i] {
			return false
		}
	}
	return true
}

func TestVide(t *testing.T) {
	var a []int = []int{}
	InsertPlace(&a)
	if len(a) > 0 {
		t.Fail()
	}
}

func TestNil(t *testing.T) {
	var a []int = nil
	InsertPlace(&a)
	if len(a) > 0 {
		t.Fail()
	}
}

func TestNormal1(t *testing.T) {
	var a []int = []int{4, 3, 2, 1}
	InsertPlace(&a)
	if !tabloEgal(a, []int{4, 3, 2, 1}) {
		t.Fail()
	}
}
