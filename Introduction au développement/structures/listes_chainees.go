package structures

import (
	"fmt"
)

type Chienne struct {
	valeur   int
	suivante *Chienne
}

func (chienne *Chienne) Tete() int {
	return chienne.valeur
}

func (chienne *Chienne) Queue() *Chienne {
	return chienne.suivante
}

func (chienne *Chienne) AjouterDebut(valeur int) *Chienne {
	return &Chienne{valeur, chienne}
}

func (chienne *Chienne) Afficher() {
	fmt.Println(chienne.valeur)
	if chienne.suivante != nil {
		chienne.suivante.Afficher()
	}
}

func EstVide(chienne *Chienne) bool {
	return chienne == nil
}

func (chienne *Chienne) Supprimer(x int) *Chienne {
	var chien *Chienne = chienne
	var prec *Chienne = chienne
	if (*chienne).valeur == x {
		return chienne.suivante
	}
	for (*chienne).suivante != nil {
		if (*chienne).valeur != x {
			*prec = *chienne
			*chienne = *(*chienne).suivante
		} else {
			(*prec).suivante = (*chienne).suivante
		}
	}
	return chien
}

func (chienne *Chienne) DansLaChienne(x int) bool {
	if chienne.valeur == x {
		return true
	}
	if chienne.Queue() == nil {
		return false
	}
	return chienne.Queue().DansLaChienne(x)
}
