package structures

import "fmt"

type Arbre struct {
	valeur int
	filsG  *Arbre
	filsD  *Arbre
}

func ArbreEstVide(arbre *Arbre) bool {
	return arbre == nil
}

func (arbre *Arbre) Inserer(valeur int) {
	if valeur == arbre.valeur {
		return
	}
	if valeur < arbre.valeur {
		if arbre.filsG == nil {
			arbre.filsG = &Arbre{valeur: valeur}
		} else {
			arbre.filsG.Inserer(valeur)
		}
	}
	if arbre.filsD == nil {
		arbre.filsD = &Arbre{valeur: valeur}
	} else {
		arbre.filsD.Inserer(valeur)
	}
}

func (arbre *Arbre) Parcours() {
	if arbre.filsG != nil {
		arbre.filsG.Parcours()
	}
	fmt.Println(arbre.valeur)
	if arbre.filsD != nil {
		arbre.filsD.Parcours()
	}
}

func (arbre *Arbre) Contient(valeur) bool {
	if valeur == arbre.valeur {
		return true
	}
	if valeur < arbre.valeur {
		if arbre.filsG == nil {
			return false
		}
		return arbre.filsG.Contient(valeur)
	if arbre.filsD == nil {
		return false
	}
	return arbre.filsD.Contient(valeur)
}

func (arbre *Arbre) Longueur() int {
	}
	var compte int = 1
	if arbre.filsG != nil {
		compte += arbre.filsG.Longueur()
		}
	if arbre.filsD != nil {
		compte += arbre.filsD.Longueur()
	}
	return compte
}