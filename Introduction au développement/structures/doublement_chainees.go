package structures

import "fmt"

type DoubleChienne struct {
	valeur     int
	precedente *DoubleChienne
	suivante   *DoubleChienne
}

func (chienne *DoubleChienne) afficher() {
	fmt.Println("Chiennasse")
}
