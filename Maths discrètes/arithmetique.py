

def division_euclidienne(a, b):
    q = 0
    while a >= b:
        a -= b
        q += 1
    return q, a

reste = lambda a, b : division_euclidienne(a,b)[1]
divisible = lambda a, b : reste(a, b) == 0
divsieurs = lambda a : [i for i in range(1, a+1) if divisible(a, i)]

def premier(a):
    for i in range(2, a//2):
        if divisible(a, i):
            return False
    return True

def tous_les_premiers(a):
    res = [2]
    for i in range(3, a+1, 2):
        if premier(i):
            res.append(i)
    return res
tous_les_premiers = lambda a : [i for i in range(2, a+1) if premier(i)]

def euclide(a, b):
    while a % b:
        _, r = division_euclidienne(a, b)
        a = b
        b = r
    return b

def algo(a, n):
    nb  = n[0]
    x = 0
    for i, el in enumerate(n[1:]):
        if euclide(el, n[i]) != 1:
            return 0, 0
        nb *= el



def billes():
    for i in range(0, 31, 3):
        if i%2 and i%5 == 2:
            return i

print(billes())
print(tous_les_premiers(7_000))

assert divisible(12,4)