import random
from arithmetique import *

def test_division_euclidienne():
    for _ in range(1000):
        a = random.randint(0, 10_000)
        b = random.randint(1, 10_000)
        assert division_euclidienne(a, b) == divmod(a, b)