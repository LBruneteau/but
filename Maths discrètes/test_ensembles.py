from ensembles import *

def test_membre():
    assert membre(1, {1, 2, 3, 5})
    assert not membre(1, set())
    assert not membre(3, {4, 5, 6})

def test_somme():
    assert somme({1, 2, 3}) == 6
    assert not somme({})
    assert somme({1, 1, 1, 1}) == 1

def test_taille():
    assert not taille({})
    assert taille({1, 2, 3}) == 3
    assert taille({1, 1, 1}) == 1

def test_est_ensemble():
    assert est_ensemble([4, 5, 6])
    assert not est_ensemble([1, 2, 3, 1])
    assert not est_ensemble([1, 1])
    assert est_ensemble([])

def test_suppr_doublon():
    assert not suppr_doublon([])
    assert suppr_doublon([1, 1, 1, 2, 5, 8, 4, 6]) == {1, 2, 5, 8, 4, 6}
    assert len(suppr_doublon([11, 11, 3, 5])) == 3

def test_inclus():
    assert inclus({}, {})
    assert inclus({}, {4, 5, 786, 486,  7 ,89})
    assert inclus({1, 3}, {1, 3, 4, 5})
    assert inclus({1, 3}, {1, 3})
    assert not inclus({1, 4}, {1, 2, 3})

def test_parties():
    assert parties({}) == {frozenset()}
    parties_123 = (set(), {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3})
    assert parties({1, 2, 3}) == {frozenset(i) for i in parties_123}