def affiche_col(t):
    for i in t:
        print(i)

def membre(x, E):
    for i in E:
        if x == i:
            return True
    return False

def somme(E):
    s = 0
    for i in E:
        s += i
    return s

def taille(E):
    t = 0
    for _ in E:
        t += 1
    return t

def est_ensemble(l):
    for i, el in enumerate(l[:-1]):
        for j in l[i+1:]:
            if j == el:
                return False
    return True

def suppr_doublon(l):
    s = set()
    for i in l:
        s.add(i)
    return s

def inclus(a, b):
    for i in a:
        if not membre(i, b):
            return False
    return True

def egal(a, b):
    return inclus(b, a) and inclus(a, b)

def intersection(a, b):
    s = set()
    for i in a:
        if i in b:
            s.add(i)
    return s

def disjoints(a, b):
    return not intersection(a, b)

def union(a,b):
    for i in b:
        a.add(i)
    return a

def ajout(x, e):
    e.add(x)
    return e

def retire(x, e):
    e.remove(x)
    return e

def diff(a, b):
    for i in a:
        if i in b:
            a.remove(i)
    return a

def diff_sym(a, b):
    return diff(union,(a, b), intersection(a, b))

def parties(a):
    p = {frozenset()}
    p.add(frozenset(a)) 
    if len(a) <= 1:
        return p
    for i in a:
        copie = set(a)
        copie.remove(i)
        sous_part = parties(copie)
        p = p.union(frozenset(sous_part))
    return p
