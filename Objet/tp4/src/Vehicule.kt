abstract class Vehicule(mod: String,coul: String, vitMax: Double)  : Propriete{
    private val modele : String
    private var couleur : String
    private var vitesseCourante : Double = 0.0
    private val vitesseMaximum : Double
    private var enMarche: Boolean = false
    private var conducteur : Personne? = null
    private var proprietaire : Proprietaire? = null

    init{
      modele = mod
      couleur = coul
      vitesseMaximum = vitMax
    }

    fun devientConducteur(personne: Personne){
      conducteur = personne
    }

    fun plusDeConducteur(){
      arreter()
      conducteur = null
    }

    fun demarrer(){
      if (conducteur != null) enMarche = true
    }

    fun arreter(){
      vitesseCourante = 0.0
      enMarche = false
    }

    fun repeindre(nouvelleCouleur: String){
      couleur = nouvelleCouleur
    }

    open fun accelerer(acceleration : Double) : Double {
        vitesseCourante = when {
            !enMarche -> vitesseCourante
            acceleration < 0.0 -> vitesseCourante
            acceleration + vitesseCourante > vitesseMaximum -> vitesseMaximum
            else -> vitesseCourante + acceleration
        }
        return vitesseCourante
    }

    open fun decelerer(deceleration : Double) : Double {
        vitesseCourante = when {
            !enMarche -> vitesseCourante
            deceleration < 0.0 -> vitesseCourante
            vitesseCourante - deceleration < 0.0 -> vitesseCourante
            else -> vitesseCourante - deceleration
        }
        return vitesseCourante
    }

    override fun acheter(acheteur: Proprietaire){
      proprietaire = acheteur
    }

    fun afficher(){
      println("Flemme.")
    }

    fun estEnMarche() : Boolean = enMarche
}
