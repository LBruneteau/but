class Camion(mod: String,coul: String, places: Int = 1) : Vehicule(mod, coul, 90.0) {

    private var placesOccupees : Int = 0
    private var remorque : Array<Voiture?>

    init{
        remorque = arrayOfNulls(places)
    }


    fun estPlein() : Boolean = placesOccupees == remorque.size
    fun estVide() : Boolean = placesOccupees == 0

    fun charger(voitureTransportee : Voiture) : Boolean{
        if (estPlein() || voitureTransportee in remorque || voitureTransportee.estEnMarche()){
            return false
        }
        remorque[placesOccupees] = voitureTransportee
        placesOccupees++
        return true
    }

    fun decharger() : Voiture?{
        if (estVide()){
            return null
        }
        placesOccupees--
        val voit : Voiture? = remorque[placesOccupees]
        remorque[placesOccupees] = null
        return voit
    }

    override fun accelerer(acceleration : Double) : Double{
        return when{
            estVide() -> super.accelerer(acceleration)
            else -> super.accelerer(acceleration / 2.0)
        }
    }

    override fun decelerer(deceleration : Double) : Double{
        return when{
            estVide() -> super.decelerer(deceleration)
            else -> super.decelerer(deceleration / 3)
        }
    }
}
