class Personne(nom : String, prenom: String) : Proprietaire{
    private val prenom : String
    private val nom : String

    init{
        this.prenom = prenom
        this.nom = nom
    }

    override fun donneNomComplet() : String{
        return "${this.prenom} ${this.nom.uppercase()}"
    }
}
