class VoitureTurbo(mod: String, coul: String, vitMax: Double) : Voiture(mod, coul, vitMax)  {

    private var turbo : Boolean = false
    private var vitesseCourante : Double = 0.0

    fun changeTurbo(etat: Boolean){
        turbo = etat
    }

    override fun accelerer(acceleration : Double) : Double{
      return when {
        turbo -> super.accelerer(acceleration * 3.0)
        else -> super.accelerer(acceleration)
      }
    }
}
