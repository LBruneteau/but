class Parking(raisonSociale: String, gerant: Personne, places: Int)
: Entreprise(raisonSociale, "SA", gerant) {

    private var stationnement : Array<Voiture?>

    init{
        stationnement = arrayOfNulls(places)
    }

    fun nombreDePlacesLibres() : Int{
        return stationnement.count({it == null})
    }

    fun nombreDePlacesTotales() : Int{
        return stationnement.size
    }

    fun placeLibre(numeroPlace : Int) : Boolean{
        if (!stationnement.indices.contains(numeroPlace)){
            return false
        }
        return stationnement[numeroPlace] == null
    }

    fun stationner(numeroPlace : Int, voitureStationnee : Voiture) : Boolean{
        if (!placeLibre(numeroPlace) || voitureStationnee in stationnement){
            return false
        }
        stationnement[numeroPlace] = voitureStationnee
        return true
    }
}
