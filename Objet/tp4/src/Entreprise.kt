open class Entreprise(raisonSociale: String, forme: String = "SCOP", gerant: Personne) : Proprietaire, Propriete  {
    private var gerant: Proprietaire
    private val raisonSociale : String
    private val forme : String

    init{
      this.raisonSociale = raisonSociale
      if (forme.uppercase() in arrayOf<String>("SCOP", "SA", "EURL")){
        this.forme = forme.uppercase()
      } else {
        this.forme = "SCOP"
      }
      this.gerant = gerant
    }

    override fun donneNomComplet() : String = "$raisonSociale $forme"
    fun donneForme() : String  = forme
    fun donneGerantActuel() : Proprietaire = gerant

    override fun acheter(acheteur : Proprietaire){
      gerant = acheteur
    }
}
