class Camion (places: Int){

    private val remorque : Array<Voiture?>
    private var places : Int = 0

    init{
        remorque = arrayOfNulls(places)
    }
    fun estPlein() : Boolean {
        return capacite() == places
    }

    fun placesLibres() : Int {
        return remorque.count({it == null})
    }

    fun placesOccupees() : Int {
        return remorque.count({it != null})
    }

    fun capacite() : Int {
        return remorque.size
    }

    fun estVide() : Boolean{
        return places == 0
    }

    fun charger(voitureTransportee: Voiture) : Boolean{
        if (estPlein() || voitureTransportee in remorque || voitureTransportee.estEnMarche()){
            return false
        }
        remorque[places] = voitureTransportee
        places++
        return true
    }

    fun decharger(): Voiture?{
        if (estVide()){
            return null
        }
        places--
        val voiture = remorque[places]
        remorque[places] = null
        return voiture
    }
}
