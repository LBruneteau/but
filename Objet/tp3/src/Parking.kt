class Parking(places: Int) {
    private val places : Array<Voiture?>

    init{
        this.places = arrayOfNulls<Voiture>(places)
    }

    fun nombreDePlacesLibres() : Int {
        return places.count({it == null})
    }

    fun nombreDePlacesOccupees() : Int {
        return nombreDePlacesTotales() - nombreDePlacesLibres()
    }

    fun nombreDePlacesTotales() : Int {
        return places.size
    }

    fun placeLibre(numeroPlace: Int) : Boolean {
        if (numeroPlace < 0 || numeroPlace >= places.size) {
            return false
        }
        return places[numeroPlace] == null
    }

    fun stationner(numeroPlace: Int, voitureStationnee: Voiture) : Boolean{
        if (placeLibre(numeroPlace) && !(voitureStationnee in places)){
            places[numeroPlace] = voitureStationnee
            return true
        }
        return false
    }
}