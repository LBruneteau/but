class Personne (prenom: String, nom: String){
    private var nom : String
    private var prenom : String

    init {
        this.prenom = prenom
        this.nom = nom
    }

    fun donneNomComplet(): String{
        return this.prenom + " " + this.nom.uppercase()
    }
}