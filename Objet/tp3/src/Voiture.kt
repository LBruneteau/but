class Voiture(mod: String, coul: String, vitMax: Double) {
    private val modele : String
    private var couleur : String
    private var vitesseCourante : Double = 0.0
    private val vitesseMaximum: Double
    private var enMarche : Boolean = false
    private var proprietaire : Personne?
    private var parking : Parking?

    init{
        modele = mod
        couleur = coul
        vitesseMaximum = vitMax
        proprietaire = null
        parking = null
    }

    fun acheter(acheteur: Personne) {
        proprietaire = acheteur
    }

    fun demarrer(){
        enMarche = true
    }

    fun arreter(){
        enMarche = false
        vitesseCourante = 0.0
    }

    fun estEnMarche() : Boolean {
        return enMarche
    }

    fun repeindre(nouvelleCouleur: String) {
        couleur = nouvelleCouleur
    }

    fun accelerer(acceleration : Double) : Double {
        vitesseCourante = when {
            !enMarche -> vitesseCourante
            acceleration < 0.0 -> vitesseCourante
            acceleration + vitesseCourante > vitesseMaximum -> vitesseMaximum
            else -> vitesseCourante + acceleration
        }
        return vitesseCourante
    }

    fun decelerer(deceleration : Double) : Double {
        vitesseCourante = when {
            !enMarche -> vitesseCourante
            deceleration < 0.0 -> vitesseCourante
            vitesseCourante - deceleration < 0.0 -> vitesseCourante
            else -> vitesseCourante - deceleration
        }
        return vitesseCourante
    }

    fun estGaree() : Boolean {
        return parking != null
    }

    fun stationner(nouveauParking : Parking) {
        if (enMarche){
            arreter()
        }
        parking = nouveauParking
    }

    fun afficher() {
        println("Monmodule UwU")
    }

    fun quitterStationnement(){
        parking = null
        demarrer()
    }
}