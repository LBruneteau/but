import kotlin.random.Random

fun main() {
    val anti = "anticonstitutionnellement"
    println(anti.length)
    println("${anti[2]}, ${anti[4]}, ${anti[19]}")
    for (i in anti.length-1 downTo 0) {
        println(anti[i])
    }
    println(anti.substring(1, 4))
    println(anti.indexOf("n"))
    println(anti.indexOf("%")) // Retourne -1
    var bjr : String = "bonjour"
    bjr = bjr.plus(" tout")
    bjr = bjr.plus(" le monde")
    println(bjr)
    for (i in 0 until 25){
        bjr = bjr.plus(anti[Random.nextInt(anti.length)])
    }
    println(bjr)
    val minu = anti.lowercase()
    println(minu)
    val maju = bjr.uppercase()
    println(maju)
}
