/*
La fonction chiffrement renvoie une chaine de caractère chiffrée en ayant exécutée
un décalage de caractères.

@param original : la chaine à chiffrer
@param decalage : le décalage à appliquer

@return la chaine chiffrée (en majuscule)
*/

fun chiffrement(original : String, decalage : Int) : String {

    val origin = original.uppercase()
    val alpha : String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    var chiffre = ""
    for (i in origin){
        if (i in alpha){
        var c : Int = (alpha.indexOf(i) + decalage).mod(26)
        chiffre = chiffre.plus(alpha[c])
        } else {
            chiffre = chiffre.plus(i)
        }
    }

    return chiffre
}