
fun main() {
    var maSaxo = Vehicule("Saxo","rouge",4,180.0)
    println(maSaxo)
    maSaxo.demarrer()
    maSaxo.accelerer(50.0)
    println(maSaxo)
    println(maSaxo.vitesse())
    var maDeuxiemeSaxo = Vehicule("Saxo", "vert", 4, 180.0)
    maDeuxiemeSaxo.demarrer()
    maDeuxiemeSaxo.accelerer(69.420)
    if (maSaxo.vaPlusVite(maDeuxiemeSaxo)){
        println("La Saxo rouge est la plus rapide")
    }
    else {
        println("La Saxo verte est la plus rapide")
    }
}
