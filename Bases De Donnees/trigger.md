# SQL dans un langage de programmation - Triggers

*Louis Bruneteau* (`i2a08b`)

## Tableau des triggers

| Nom du trigger              | Type : `BEFORE` ou `AFTER` | `INSERT`, `DELETE`, `UPDATE` | Nom de la table       | `FOR EACH ROW` : oui ou non |
| --------------------------- | -------------------------- | ---------------------------- | --------------------- | --------------------------- |
| *pas_diminuer_salaire*      | `AFTER`                    | `UPDATE`                     | *employe*             | Oui                         |
| *pas_augmenter_hebdo*       | `AFTER`                    | `UPDATE`                     | *employe*             | Oui                         |
| *del_travail_employe*       | `AFTER`                    | `DELETE`                     | *travail*             | Non                         |
| *supprimer_projet*          | `AFTER`                    | `DELETE`                     | *travail*, *concerne* | Non                         |
| **travail_trop_important*   | `AFTER`                    | `INSERT`, `UPDATE`           | *travail*             | Non                         |
| *reduction_temps_travail*   | `AFTER`                    | `UPDATE`                     | *employe*             | Non                         |
| *resp_trois_projets_max*    | `AFTER`                    | `INSERT`, `UPDATE`           | *projet*              | Non                         |
| *trois_projets_par_service* | `AFTER`                    | `INSERT`, `UPDATE`           | *concerne*            | Non                         |

## Exercice 1 : Triggers de type `FOR EACH ROW` et utilisation de `:NEW` et `:OLD`

### A. Écrire un trigger interdisant la diminution du salaire

```sql
CREATE TRIGGER pas_diminuer_salaire AFTER
    UPDATE of salaire ON employe FOR EACH ROW
    BEGIN
    if :new.salaire < :old.salaire then
    raise_application_error
    (-20001, 'un salaire ne peut être diminué');
    END IF;
    END;
```

La contrainte a été testée en tentant de diminuer le salaire des employés.

```sql
UPDATE employe set salaire = 0;
```

L'exécution de la requête a été empêchée par le déclencheur.

```
Erreur commençant à la ligne: 1 de la commande -
UPDATE employe set salaire = 0
Rapport d'erreur -
ORA-20001: un salaire ne peut être diminué
ORA-06512: à "I2A08B.PAS_DIMINUER_SALAIRE", ligne 3
ORA-04088: erreur lors d'exécution du déclencheur 'I2A08B.PAS_DIMINUER_SALAIRE'
```

### B. Trigger empêchant l'augmentation des heures hebdomadaires des employés

```sql
CREATE TRIGGER pas_augmenter_hebdo AFTER
    UPDATE of hebdo ON employe FOR EACH ROW
    BEGIN
    if :new.hebdo > :old.hebdo then
    raise_application_error
    (-20002, 'une durée hebdomadaire ne peut être augmentée');
    END IF;
    END;
```

On peut tester le trigger avec une requête qui double les heures des cinq premiers employés dans la table.

```sql
UPDATE employe set hebdo = 2*hebdo WHERE ROWNUM <= 5;
```

Mais la modification est rejetée par Oracle.

```
Erreur commençant à la ligne: 1 de la commande -
UPDATE employe set hebdo = 2*hebdo WHERE ROWNUM <= 5
Rapport d'erreur -
ORA-20002: une durée hebdomadaire ne peut être augmentée
ORA-06512: à "I2A08B.PAS_AUGMENTER_HEBDO", ligne 3
ORA-04088: erreur lors d'exécution du déclencheur 'I2A08B.PAS_AUGMENTER_HEBDO'
```

## Exercice 2 : Trigger de type : `DELETE FROM T2 WHERE a NOT IN (SELECT a FROM T1)`

### A. Supprimer un employé et ses enregistrements associés dans *travail*

```sql
CREATE TRIGGER del_travail_employe AFTER
    DELETE ON employe
BEGIN
    DELETE FROM travail t
    WHERE
        t.nuempl NOT IN (
            SELECT
                e.nuempl
            FROM
                employe e
        );

END;
```

Pour tester cette requête j'ai choisi un employé qui a un enregistrement associé dans la table *travail*

```sql
SELECT
    *
FROM
    employe   e
    INNER JOIN travail   t ON t.nuempl = e.nuempl
WHERE
    e.nuempl = 42;
```

| nuempl | nomempl | hebdo | affect | salaire | nuproj | duree |
| ------ | ------- | ----- | ------ | ------- | ------ | ----- |
| 42     | albert  | 25    | 1      | 2000    | 135    | 30    |

Ensuite, j'ai supprimé Albert.

```sql
DELETE FROM employe WHERE nuempl = 42;
```

La requête suivante n'a rien retourné, confirmant que le travail a été supprimé de la table.

```sql
SELECT * FROM travail WHERE nuempl = 42;
```

### B. Suppression d'un projet et des ses références dans `concerne` et `travail`

J'ai créé un trigger qui supprime les enregistrements de la table `concerne` et de la table `travail` à la suppression d'un projet.

```sql
CREATE TRIGGER supprimer_projet AFTER
    DELETE ON projet
BEGIN
    DELETE FROM travail t
    WHERE
        t.nuproj NOT IN (
            SELECT
                p.nuproj
            FROM
                projet p
        );
    DELETE FROM concerne c
    WHERE
        c.nuproj NOT IN (
            SELECT p.nuproj FROM projet p
        );

END;
```

Le test fut de supprimer le projet *esprit*.

```sql
SELECT * FROM projet WHERE nuproj = 160;
```

| NUPROJ | NOMPROJ | RESP |
| ------ | ------- | ---- |
| 160    | esprit  | 30   |

```sql
DELETE FROM projet WHERE nuproj = 160;
```

Ensuite les requêtes de sélection ci-dessous n'ont rien retourné, indiquant la réussite de la suppression par le trigger.

```sql
SELECT * FROM travail WHERE nuproj = 160;
SELECT * FROM concerne WHERE nuproj = 160;
```

## Exercice 3

### A. La somme des durées de travail d'un employé ne doit pas excéder son temps de travail hebdomadaire

#### 1. Combien d'opérations mènent à ne pas respecter cette contrainte ?

Un employé peut se retrouver avec une somme des durées supérieures à son temps hebdomadaire de trois façons différentes.

- Si on insère un nouveau travail dans la table 
- Si on change la durée d'un travail existant
- Si on diminue le temps de travail hebdomadaire d'un employé

#### 2. Combien de triggers allez-vous mettre en place ?

Il faudra mettre en place deux triggers pour ces différents cas de figure. Ils se déclencheront à l'insertion d'un travail demandant trop d'heures à un employé, ou sur un update de `travail.duree` ou de `employe.hebdo` si la somme des travaux dépasse la durée hebdomadaire. Un trigger concernera les modifications et les insertions de la table `travail`, l'autre les modifications de la table `employe`

#### 3. Construire les triggers

##### Trigger de la table `travail`

```sql
CREATE OR REPLACE TRIGGER travail_trop_important
AFTER INSERT OR UPDATE OF duree ON travail
DECLARE emp employe%rowtype;
BEGIN
    SELECT e.* INTO emp FROM employe e
    WHERE hebdo < 
    (SELECT sum(duree) FROM travail t WHERE t.NUEMPL = e.NUEMPL);
raise_application_error(-20003, 'Impossible de rajouter du travail à cet employé, sa durée hebdomadaire est trop basse');
EXCEPTION
WHEN no_data_found THEN NULL;
WHEN too_many_rows THEN
raise_application_error(-20004, 'Impossible de rajouter du travail à cet employé, sa durée hebdomadaire est trop basse');
END;
```

À présent, si j'essaie de donner trop de travail au pauvre Marcel, la base de données le protégera en déclanchant le trigger.

```sql
UPDATE travail SET duree=69 WHERE nuempl IN 
(SELECT nuempl FROM employe WHERE NOMEMPL = 'marcel');
```

```
SQL Error [20004] [72000]: ORA-20004: Impossible de rajouter du travail à cet employé, sa durée hebdomadaire est trop basse
ORA-06512: à "I2A08B.TRAVAIL_TROP_IMPORTANT", ligne 10
ORA-04088: erreur lors d'exécution du déclencheur 'I2A08B.TRAVAIL_TROP_IMPORTANT'
```

##### Trigger de la table `employe`

```sql
CREATE TRIGGER reduction_temps_travail
AFTER UPDATE OF hebdo ON employe
DECLARE emp employe%rowtype;
BEGIN
    SELECT e.* INTO emp FROM EMPLOYE e 
    WHERE hebdo < 
    (SELECT sum(duree) FROM travail t WHERE t.nuempl = e.nuempl);
raise_application_error(-20005, 'Impossible de diminuer le temps de travail hebdomadaire de cet employé.');
EXCEPTION
WHEN no_data_found THEN NULL;
WHEN too_many_rows THEN
raise_application_error(-20006, 'Impossible de diminuer le temps de travail hebdomadaire de cet employé.');
END;
```

Nous pouvons tester ce trigger en diminuant le temps de travail hebdomadaire d'un employé sans changer sa charge de travail.

```sql
UPDATE employe SET hebdo=1 WHERE nomempl='marcel';
```

```
SQL Error [20006] [72000]: ORA-20006: Impossible de diminuer le temps de travail hebdomadaire de cet employé.
ORA-06512: à "I2A08B.REDUCTION_TEMPS_TRAVAIL", ligne 10
ORA-04088: erreur lors d'exécution du déclencheur 'I2A08B.REDUCTION_TEMPS_TRAVAIL'
```

Marcel devra malheuseusement travailler plus d'une heure dans sa semaine pour finir ses tâches. Le trigger de la base de données l'a dit. C'est très dommage.

### B. Un employé est responable sur au plus 3 projets.

Il faut écrire un trigger vérifiant à l'insertion d'un projet ou à la modification de la colonne `resp` si un seul employé est responable sur plus de trois projets.

```sql
CREATE TRIGGER resp_trois_projets_max
AFTER INSERT OR UPDATE OF resp ON projet
DECLARE emp employe%rowtype;
BEGIN
    SELECT e.* INTO emp FROM employe e
    WHERE 3 < 
    (SELECT COUNT(*) FROM projet WHERE resp = e.NUEMPL);
raise_application_error(-20007, 'Un employé est responable sur au plus trois projets.');
EXCEPTION
WHEN no_data_found THEN NULL;
WHEN too_many_rows THEN
raise_application_error(-20008, 'Un employé est responable sur au plus trois projets.');
END;
```

Edith, l'employé numéro 30, est responable de deux projets. Pour tester le trigger je vais lui en rajouter.

```sql
INSERT INTO projet VALUES(727, 'WYSI', 30);
```

Après cette requête, Edith a trois projets. Le trigger n'est donc pas déclenché. Cependant, après l'insertion suivante, Edith a plus de trois projets. L'opération est donc refusée.

```sql
INSERT INTO projet VALUES(343, 'Amogus', 30);
```

```
SQL Error [20007] [72000]: ORA-20007: Un employé est responable sur au plus trois projets.
ORA-06512: à "I2A08B.RESP_TROIS_PROJETS_MAX", ligne 6
ORA-04088: erreur lors d'exécution du déclencheur 'I2A08B.RESP_TROIS_PROJETS_MAX'
```

### C. Un service ne peut être concerné par plus de 3 projets.

Pour ce trigger il faudra utiliser la table `concerne` qui fait le lien entre les services et les projets. 

```sql
CREATE TRIGGER trois_projets_par_service
AFTER INSERT OR UPDATE ON concerne
DECLARE serv service%rowtype;
BEGIN
	SELECT s.* INTO serv FROM service s
	WHERE 3 < 
	(SELECT COUNT(*) FROM concerne c WHERE c.NUSERV = s.NUSERV);
raise_application_error(-20009, 'Un service peut être associé à trois projets au plus.')
EXCEPTION
WHEN no_data_found THEN NULL;
WHEN too_many_rows THEN
raise_application_error(-20010, 'Un service peut être associé à trois projets au plus.');
END;
```

On peut ensuite tester ce trigger en affectant plus de projets à un service. Le service 1 a exactement 3 projets. Lui en affecter un quatrième devrait déclencher le trigger.

```sql
UPDATE concerne SET nuserv=1 WHERE NUSERV = 2 AND NUPROJ = 237;
```

```
SQL Error [20010] [72000]: ORA-20010: Un service peut être associé à trois projets au plus.
ORA-06512: à "I2A08B.TROIS_PROJETS_PAR_SERVICE", ligne 10
ORA-04088: erreur lors d'exécution du déclencheur 'I2A08B.TROIS_PROJETS_PAR_SERVICE'
```