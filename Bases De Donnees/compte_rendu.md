# Compte-rendu d'exploitation de bases de données R2/06, TD 2
*Louis Bruneteau*

## Schéma relationnel de la base de données

Distribution(**ID**, *#NUM_FO*, *#CODE_INSEE*, ADRESSE, STATUT, ...);  
Operateur(**NUMFO**, NOMFO, GENERATION, TECHNOLOGIE);  
Commune(**CODE_INSEE**, NOMCOMMUNE, *#NOMDEP*);  
Departement(**NOMDEP**, CODE_DEPARTEMENT);  

## Requêtes

### 1. Liste des communes qui se trouvent dans le département 49
Il s'agit de faire une jointure entre les tables `commune` et `departement`   
#### Avec un `IN`
```sql
SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    nomdep IN (
        SELECT
            nomdep
        FROM
            basetd.departement
        WHERE
            code_departement = 49
    )
ORDER BY
    1;
```

#### Avec un `EXISTS`
```sql
SELECT
    nom_commune
FROM
    basetd.commune c
WHERE
    EXISTS (
        SELECT
            *
        FROM
            basetd.departement d
        WHERE
            code_departement = 49
            AND c.nomdep = d.nomdep
    )
ORDER BY
    1;
```

### 2. Liste des noms des communes avec le nom du département qui disposent de la 5G
Il faut faire des jointures entre les tables `commune`, `distribution` et `operateur` pour ne sélectionner que les antennes *5G* dans la sous-requête.  
#### Avec un `IN`
```sql
SELECT DISTINCT
    c.nom_commune,
    c.nomdep
FROM
    basetd.commune c
WHERE
    c.code_insee IN (
        SELECT
            code_insee
        FROM
            basetd.distribution d
        WHERE
            d.numfo IN (
                SELECT
                    numfo
                FROM
                    basetd.operateur o
                WHERE
                    o.numfo = d.numfo
                    AND generation = '5G'
            )
    )
ORDER BY
    1;
```

#### Avec un `EXISTS`
```sql
SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    EXISTS (
        SELECT
            *
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
            AND di.code_insee = c.code_insee
    )
ORDER BY
    1;
```

### 3. Liste des communes avec le nom du département qui ne possèdent pas la 5G
Cette requête est la même que la précédente sauf que le `IN` est remplacé par un `NOT IN` afin de sélectionner une commune que s'il n'y a aucune antenne de génération *5G* installée à cet endroit.  
#### Avec un `NOT IN`
```sql
SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    c.code_insee NOT IN (
        SELECT
            code_insee
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
    )
ORDER BY
    1;
```
#### Avec un `NOT EXISTS`  
```sql
SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
            AND di.code_insee = c.code_insee
    )
ORDER BY
    1;
```
### 4. Liste des communes qui ne possèdent pas la 5G et qui se trouvent dans le département 44
Cette requête est ma même que la précédente sauf que l'on rajoute une condition dans le `WHERE` de la requête principale pour ne garder que les communes dans le 44.  
#### Avec un `NOT IN`
```sql
SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    c.code_insee NOT IN (
        SELECT
            code_insee
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
    AND d.code_departement = 44
ORDER BY
    1;
```

#### Avec un `NOT EXISTS`

```sql
SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
            AND di.code_insee = c.code_insee
    )
        AND d.code_departement = 44
ORDER BY
    1;
```

### 5. Afficher pour chaque département le nombre de communes. Faites une jointure de la table commune et celle du département.  

#### 1ère forme : `INNER JOIN` et `GROUP BY`
En faisant une jointure entre les tables `commune` et `departement` et en utilisant `GROUP BY` pour regrouper les communes par département on peut trouver le nombre de communes en utilisant `COUNT(*)`
```sql
SELECT
    d.nomdep,
    COUNT(*)
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
GROUP BY
    d.nomdep
ORDER BY
    1;
```

#### 2ème forme: Avec une sous-requête
Il est également possible de faire une jointure entre les deux tables en utilisant une sous-requête. Le nombre de département est également donné en utilisant `COUNT(*)`, mai (sous deux formes)s depuis la sous-requête.
```sql
SELECT
    d.nomdep,
    (
        SELECT
            COUNT(*)
        FROM
            basetd.commune c
        WHERE
            c.nomdep = d.nomdep
    ) compte
FROM
    basetd.departement d
ORDER BY
    1;
```

### 6. Liste de toutes les communes de Loire-Atlantique avec le nombre d'antennes 5G. Le résultat est trié par ordre croissant de la deuxième colonne.
Il est possible de répondre à cette question en faisant une double jointure entre les tables `commune`, `distribution` et `operateur`.
#### 1ère forme: `INNER JOIN` et `GROUP BY`

```sql
SELECT
    c.nom_commune,
    COUNT(*)
FROM
    basetd.commune        c
    INNER JOIN basetd.distribution   d ON d.code_insee = c.code_insee
    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
WHERE
    generation = '5G'
    AND nomdep = 'Loire-Atlantique'
GROUP BY (
    c.nom_commune
)
ORDER BY
    2,
    1;
```

#### 2ème forme: Utilisation d'une sous-requête
```sql
SELECT
    *
FROM
    (
        SELECT
            c.nom_commune,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '5G'
            ) compte
        FROM
            basetd.commune c
        WHERE
            nomdep = 'Loire-Atlantique'
    )
WHERE
    compte > 0
ORDER BY
    2,
    1;
  ```

### 7. Idem que la question précédente avec au moins 10 antennes.
#### 1ère forme
Cette requête est la même que la précédente, sauf que l'on ajoute la clause `HAVING` pour ne garder que les lignes avec plus de 10 antennes 5G.
```sql
SELECT
    c.nom_commune,
    c.nomdep,
    COUNT(*)
FROM
    basetd.commune        c
    INNER JOIN basetd.distribution   d ON d.code_insee = c.code_insee
    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
WHERE
    generation = '5G'
GROUP BY (
    c.nom_commune,
    c.nomdep
)
HAVING (COUNT(*)>=10)
ORDER BY
    3,
    1;
```
#### 2ème forme
Cette requête est également la même que la précédente, mais il faut changer `WHERE compte > 0` pour avoir 10 antennes ou plus
```sql
SELECT
    *
FROM
    (
        SELECT
            c.nom_commune,
            c.nomdep,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '5G'
            ) compte
        FROM
            basetd.commune c
    )
WHERE
    compte >= 10
ORDER BY
    3,
    1;
```

### 8. Liste de toutes les communes de Loire-Atlantique avec le nombre d'antennes 5G et 4G.
Cette requête est la même que la deuxième forme de la sixième question, mais avec une seconde sous-requête extrêmement similaire à la première qui compte le nombre d'antennes 4G.
```sql
SELECT
    *
FROM
    (
        SELECT
            c.nom_commune,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '5G'
            ) "5G",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '4G'
            ) "4G"
        FROM
            basetd.commune c
        WHERE
            c.nomdep = 'Loire-Atlantique'
    )
ORDER BY
    2 DESC,
    3 DESC,
    1;
```

### 9. Liste de tous les fournisseurs avec le nombre d'antennes 5G de 3,5Ghz, 2,1 GHZ et 700MHZ(5G NR 3500, 2100 et 700)  
#### 1ère forme
Pour cette question, j'ai fait une jointure entre les tables `operateur` et `distribution` afin de pouvoir accéder à toutes les antennes. À l'aide d'un `GROUP BY` j'ai pu regrouper par type d'antenne et par fournisseur et accéder au nombre de chaque.


```sql
SELECT
    nomfo,
    technologie,
    COUNT(*) nombre
FROM
    basetd.operateur
    NATURAL JOIN basetd.distribution
WHERE
    technologie IN (
        '5G NR 3500',
        '5G NR 2100',
        '5G NR 700'
    )
GROUP BY (
    nomfo,
    technologie
);
```
Le graphique généré se trouve [ici](https://gitlab.com/LBruneteau/but/-/blob/main/Bases%20De%20Donnees/img1.png)
#### 2ème forme
Cette requête est composée de sous-requêtes comptant le nombre d'antennes de chaque type pour chaque opérateur. La requête principale est elle-même contenue dans une autre, afin de pouvoir grouper par nom d'opérateur et d'éviter d'avoir plusieurs lignes de 0.
```sql
SELECT
    nomfo,
    MAX("3500") "3500",
    MAX("2100") "2100",
    MAX("700") "700"
FROM
    (
        SELECT DISTINCT
            o.nomfo,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 3500'
                    AND o.numfo = d.numfo
            ) "3500",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 2100'
                    AND o.numfo = d.numfo
            ) "2100",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 700'
                    AND o.numfo = d.numfo
            ) "700"
        FROM
            basetd.operateur o
    )
GROUP BY (
    nomfo
);
```
Le graphique généré est [ici](https://gitlab.com/LBruneteau/but/-/blob/main/Bases%20De%20Donnees/img2.png)

### 10. Idem que la question précédente pour la ville de Nantes.
#### 1ère forme
Cette requête est la même que la précédente, sauf que l'on rajoute la condition `nom_commune = 'Nantes'` et une jointure avec la table `commune`
```sql
SELECT
    nomfo,
    technologie,
    COUNT(*) nombre
FROM
    basetd.operateur
    NATURAL JOIN basetd.distribution NATURAL JOIN basetd.commune
WHERE
    technologie IN (
        '5G NR 3500',
        '5G NR 2100',
        '5G NR 700'
    ) AND nom_commune = 'Nantes'
GROUP BY (
    nomfo,
    technologie
);
```
Le graphique est [ici](https://gitlab.com/LBruneteau/but/-/blob/main/Bases%20De%20Donnees/img3.png)
#### 2ème forme
J'ai changé la requête de la question précédente en utilisant la table `commune` et en rajoutant la condition `nom_commune = 'Nantes'`, et en faisant la jointure dans chaque sous-requête.  
```sql
SELECT
    nomfo,
    MAX("3500") "3500",
    MAX("2100") "2100",
    MAX("700") "700"
FROM
    (
        SELECT DISTINCT
            o.nomfo,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 3500'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "3500",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 2100'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "2100",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 700'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "700"
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            nom_commune = 'Nantes'
    )
GROUP BY (
    nomfo
);
```
Le graphique est [ici](https://gitlab.com/LBruneteau/but/-/blob/main/Bases%20De%20Donnees/img4.png)

### 11. Idem pour le département de Loire-Atlantique.  
#### 1ère forme
Cette requête est la même que la 10, sauf que la condition change. Ce n'est pas `nom_commune = 'Nantes'`, mais `nomdep = 'Loire-Atlantique'`.
```sql
SELECT
    nomfo,
    technologie,
    COUNT(*) nombre
FROM
    basetd.operateur
    NATURAL JOIN basetd.distribution NATURAL JOIN basetd.commune
WHERE
    technologie IN (
        '5G NR 3500',
        '5G NR 2100',
        '5G NR 700'
    ) AND nomdep = 'Loire-Atlantique'
GROUP BY (
    nomfo,
    technologie
);
```
Le graphique est [ici](https://gitlab.com/LBruneteau/but/-/blob/main/Bases%20De%20Donnees/img5.png)

#### 2ème forme
Cette requête est la même que la 10, sauf qu'une fois de plus, la condition change.
```sql
SELECT
    nomfo,
    MAX("3500") "3500",
    MAX("2100") "2100",
    MAX("700") "700"
FROM
    (
        SELECT DISTINCT
            o.nomfo,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 3500'
                    AND o.numfo = d.numfo AND c.code_insee = d.code_insee
            ) "3500",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 2100'
                    AND o.numfo = d.numfo AND c.code_insee = d.code_insee
            ) "2100",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 700'
                    AND o.numfo = d.numfo AND c.code_insee = d.code_insee
            ) "700"
        FROM
            basetd.operateur o, basetd.commune c WHERE c.nomdep = 'Loire-Atlantique'
    )
GROUP BY (
    nomfo
);
```
Le graphique est [ici](https://gitlab.com/LBruneteau/but/-/blob/main/Bases%20De%20Donnees/img6.png)

### 12. Listes des communes qui possèdent le déploiement de toutes les technologies des différents opérateurs.

J'ai cherché le nom des communes qui ne faisaient pas partie de la liste des communes qui n'ont pas toutes les technologies différentes avec une double négation faite avec des `NOT IN`  
#### 1ère forme
```sql
SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    code_insee NOT IN (
        SELECT
            c.code_insee
        FROM
            basetd.operateur,
            basetd.commune c
        WHERE
            numfo NOT IN (
                SELECT DISTINCT
                    numfo
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
            )
    );
```
#### 2ème forme
J'ai légèrement modifié la requête précédente pour utiliser `NOT EXISTS` et non pas `NOT IN`, cette fois-ci en comparant les clés dans les sous-requêtes.
```sql
SELECT
    nom_commune
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            c.code_insee
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.code_insee = co.code_insee
            AND NOT EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
                    AND o.numfo = numfo
            )
    );
```

### 13. Listes des communes qui possèdent le déploiement des technologies 5G des différents opérateurs.
Cette requête est la même que la précédente, mais on vérifie dans chaque sous requête si la génération est bien la 5G  
#### 1ère forme
```sql
SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    code_insee NOT IN (
        SELECT
            c.code_insee
        FROM
            basetd.operateur,
            basetd.commune c
        WHERE
            generation = '5G'
            AND numfo NOT IN (
                SELECT DISTINCT
                    numfo
                FROM
                    basetd.operateur      op
                    NATURAL JOIN basetd.distribution   d
                WHERE
                    d.code_insee = c.code_insee
                    AND op.generation = '5G'
            )
    );
```
#### 2ème forme
```sql
SELECT
    nom_commune
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            c.code_insee
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.code_insee = co.code_insee
            AND o.generation = '5G'
            AND NOT EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
                    AND o.numfo = numfo
                    AND generation = '5G'
            )
    );
```

### 14. Listes des communes qui possèdent le déploiement de toutes les technologies 4G de l'opérateur orange.
Même requête que la précédente, mais il faut vérifier dans les deux sous-requêtesque la génération est la 4G et que le nom de l'opérateur est Orange.  
#### 1ère forme
```sql
SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    code_insee NOT IN (
        SELECT
            c.code_insee
        FROM
            basetd.operateur,
            basetd.commune c
        WHERE
            generation = '4G' AND nomfo = 'ORANGE'
            AND numfo NOT IN (
                SELECT DISTINCT
                    numfo
                FROM
                    basetd.operateur      op
                    NATURAL JOIN basetd.distribution   d
                WHERE
                    d.code_insee = c.code_insee
                    AND op.generation = '4G' AND op.nomfo = 'ORANGE'
            )
    );
```
#### 2ème forme
```sql
SELECT
    nom_commune
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            c.code_insee
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.code_insee = co.code_insee
            AND o.generation = '4G' AND o.nomfo = 'ORANGE'
            AND NOT EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
                    AND o.numfo = numfo
                    AND generation = '4G' AND nomfo = 'ORANGE'
            )
    );
```
