# R2.06 Exploitation d'une Base de Données : Mini Projet


### Auteurs (INFO 1 TP 2-1)
- Enzo CROSES : `i2b07b`
- Camille MOREAU : `i2b01b`
- Lomann LECOQ : `i2b01a`
- Louis BRUNETEAU : `i2b03a`

## 1. Décomposer cette table en plusieurs tables. Chaque relation doit être en troisième forme normale.

Nous avons choisi de diviser la base de données selon le schéma suivant.
![alt](img/entite_relation.png)

Nous avons ensuite réparti les tables entre les différents membres du groupe.
- Louis : *Borne*
- Camille : *Station*
- Lomann : *Commune*, *Département*
- Enzo : *Région*

## 2. & 3. & 4. Création des tables et des contraintes, permissions.

### Table Région
```sql
CREATE TABLE region (
    code_region   NUMBER(2, 0) PRIMARY KEY,
    nom_region    VARCHAR2(50)
);

INSERT INTO region VALUES (
    52,
    'Pays de la Loire'
);

GRANT SELECT, REFERENCES ON region TO i2b03a WITH GRANT OPTION;

GRANT SELECT, REFERENCES ON region TO i2b01a WITH GRANT OPTION;

GRANT SELECT, REFERENCES ON region TO i2b01b WITH GRANT OPTION;
```

### Table Département
```sql
CREATE TABLE departement (
    code_dep   NUMBER(2, 0) PRIMARY KEY,
    nom_dep    VARCHAR2(50),
    code_region   NUMBER(2, 0),
    FOREIGN KEY ( code_region )
        REFERENCES i2b07b.region ( code_region )
);

INSERT INTO departement VALUES (
    44,
    'Loire-Atlantique',
    52
);

INSERT INTO departement VALUES (
    49,
    'Maine-et-Loire',
    52
);

INSERT INTO departement VALUES (
    53,
    'Mayenne',
    52
);

INSERT INTO departement VALUES (
    72,
    'Sarthe',
    52
);

INSERT INTO departement VALUES (
    85,
    'Vendée',
    52
);
GRANT SELECT, REFERENCES ON departement TO i2b03a WITH GRANT OPTION;
GRANT SELECT, REFERENCES ON departement TO i2b01b WITH GRANT OPTION;
GRANT SELECT, REFERENCES ON departement TO i2b07b WITH GRANT OPTION;
```

### Table Commune
```sql
CREATE TABLE commune
    AS
        SELECT DISTINCT
            to_number(substr(insee, 1, 5)) code_insee,
            commune nom_commune,
            code_dep
        FROM
            basetd.rechargeelectrique;
```

Nous avons ensuite tenté d'ajouter la contrainte de clé primaire avec la commande suivante.
```sql
ALTER TABLE commune ADD CONSTRAINT cp_commune PRIMARY KEY(code_insee);
```
Cependant, dû à des erreurs dans la table `basetd.rechargeelectrique`, la contrainte d'unicité de la clé primaire n'était pas respectée. La requête a donc échoué, et nous avons été obligés de corriger la table manuellement, en trouvant les doublons et en changeant les codes *insee* erronés.  
Nous avons utilisé cette commande afin de détecter les codes *insee* apparaissant en double dans la table.

```sql
SELECT code_insee, COUNT(*) FROM commune GROUP BY code_insee ORDER BY 2 DESC;
```

Nous avons ainsi pu, à l'aide de recherches sur Internet, rétablir les bons codes.

```sql
SELECT * FROM commune WHERE code_insee = 85234;
SELECT * FROM commune WHERE code_insee = 85223;
SELECT * FROM commune WHERE code_insee = 85500;
SELECT * FROM commune WHERE code_insee = 85248;

UPDATE commune SET code_insee = 85500 WHERE code_insee = 85234 AND nom_commune = 'Saint-Paul';
UPDATE commune SET code_insee = 85248 WHERE code_insee = 85223 AND nom_commune = 'Saint-Martin-Lars-en-Sainte-Hermine';
```

Une fois cela fait, nous avons réussi à créer les contraintes, puis à accorder les droits aux autres membres du groupe.

```sql
ALTER TABLE commune ADD CONSTRAINT cp_commune PRIMARY KEY(code_insee);

ALTER TABLE commune ADD CONSTRAINT ce_commune FOREIGN KEY(code_dep) REFERENCES departement(code_dep);


GRANT SELECT, REFERENCES ON commune TO i2b03a WITH GRANT OPTION;
GRANT SELECT, REFERENCES ON commune TO i2b01b WITH GRANT OPTION;
GRANT SELECT, REFERENCES ON commune TO i2b07b WITH GRANT OPTION;
```

### Table Station

Nous avons créé la table à partir de `basetd.rechargeelectrique` grâce à cette requête.

```sql
CREATE TABLE station
    AS
        SELECT DISTINCT
            longitude,
            latitude,
            adresse,
            libelléstation   libelle,
            source,
            aménageur        amenageur,
            opérateur        operateur,
            enseigne,
            to_number(substr(insee, 1, 5)) code_insee
        FROM
            basetd.rechargeelectrique;
```

Une fois encore, la contrainte de la clé primaire n'était pas respectée, comme nous avons pu le remarquer en tentant de la créer.

```sql
ALTER TABLE station ADD CONSTRAINT cp_station PRIMARY KEY (adresse);
```

Nous avons donc recherché les adresses apparaissant en double pour régler les conflits.

```sql
SELECT
    adresse,
    COUNT(*)
FROM
    station
GROUP BY
    adresse
ORDER BY
    2 DESC;

SELECT
    *
FROM
    station
WHERE
    adresse = 'Avenue Maréchal Joffre 44500 La Baule-Escoublac';

DELETE FROM station
WHERE
    adresse = 'Avenue Maréchal Joffre 44500 La Baule-Escoublac'
    AND latitude = '47.285563';

DELETE FROM station
WHERE
    adresse = 'Avenue Maréchal Joffre 44500 La Baule-Escoublac'
    AND latitude = '47.285516';

SELECT
    *
FROM
    station
WHERE
    adresse = '6 Avenue Gustave Flaubert 44350 Guérande';

DELETE FROM station
WHERE
    adresse = '6 Avenue Gustave Flaubert 44350 Guérande'
    AND latitude = '47.324248';

SELECT
    *
FROM
    station
WHERE
    adresse = 'Rue Henri Matisse 44600 Saint-Nazaire';

DELETE FROM station
WHERE
    adresse = 'Rue Henri Matisse 44600 Saint-Nazaire'
    AND latitude = '47.287351';
```

Une fois ceci faits, les contraintes ont été crées.
```sql
ALTER TABLE station ADD CONSTRAINT cp_station PRIMARY KEY ( adresse );

ALTER TABLE station
    ADD CONSTRAINT ce_station FOREIGN KEY (code_insee)
        REFERENCES i2b01a.commune(code_insee);
```

Nous avons accordé aux autres utilisateurs la permission d'utiliser la table créée.

```sql
GRANT SELECT, references ON station TO i2b03a WITH GRANT OPTION;
GRANT SELECT, references ON station TO i2b01a WITH GRANT OPTION;
GRANT SELECT, references ON station TO i2b07b WITH GRANT OPTION;
```

### Table Borne

Une des difficultés rencontrées en créant cette table était les attributs `nombrepointsdecharge` et `puissance_max`. Ils représentent des valeurs numériques, mais sont codés comme des `VARCHAR2` dans la table `basetd.rechargeelectrique`. Il nous a donc fallu les convertir. La première étape était de remplacer les points séparateurs de la partie décimale par des virgules, avant d'utiliser la fonction `to_number()`.  

La formule utilisée a été

```sql
to_number(replace(nombrepointsdecharge, '.', ','))
```

Et nous avons pu nous en servir pour créer la table

```sql
CREATE TABLE borne
    AS
        SELECT DISTINCT
            codestation       id_borne,
            typedeprise       type_prise,
            to_number(replace(nombrepointsdecharge, '.', ',')) nb_charges,
            to_number(replace(puissancemaximum, '.', ',')) puissance_max,
            datedemiseajour   date_maj,
            accèsrecharge     acces,
            observations,
            adresse
        FROM
            basetd.rechargeelectrique;
```

Cette fois ci aucune ambiguité ne fut à régler lors de la création de la table.

```sql
ALTER TABLE borne ADD CONSTRAINT cp_borne PRIMARY KEY ( id_borne );

ALTER TABLE borne
    ADD CONSTRAINT ce_borne FOREIGN KEY ( adresse )
        REFERENCES i2b01b.station ( adresse );

GRANT SELECT, REFERENCES ON borne TO i2b01a WITH GRANT OPTION;

GRANT SELECT, REFERENCES ON borne TO i2b01b WITH GRANT OPTION;

GRANT SELECT, REFERENCES ON borne TO i2b07b WITH GRANT OPTION;
```

## 5.1 Le nombre de prises par aménageur par département

### 1. Loire-Atlantique (Camille)
```sql
SELECT DISTINCT
    amenageur,
    nom_commune,
    SUM(nb_charges)
FROM
    station
    NATURAL JOIN i2b03a.borne
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01a.departement
WHERE
    nom_dep = 'Loire-Atlantique'
GROUP BY (
    amenageur,
    nom_commune
)
ORDER BY
    2,
    3 DESC;
```

### 2. Sarthe (Enzo)
```sql
SELECT DISTINCT
    amenageur,
    nom_commune,
    SUM(nb_charges)
FROM
    i2b01b.station
    NATURAL JOIN i2b03a.borne
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01a.departement
WHERE
    nom_dep = 'Sarthe'
GROUP BY (
    amenageur,
    nom_commune
)
ORDER BY
    2,
    3 DESC;
```

### 3. Vendée (Lomann)
```sql
SELECT DISTINCT
    amenageur,
    nom_commune,
    SUM(nb_charges)
FROM
    i2b01b.station
    NATURAL JOIN i2b03a.borne
    NATURAL JOIN commune
    NATURAL JOIN departement
WHERE
    nom_dep = 'Vendée'
GROUP BY (
    amenageur,
    nom_commune
)
ORDER BY
    2,
    3 DESC;
```

### 4. Mayenne (Louis)
```sql
SELECT DISTINCT
    amenageur,
    nom_commune,
    SUM(nb_charges)
FROM
    i2b01b.station
    NATURAL JOIN borne
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01a.departement
WHERE
    nom_dep = 'Mayenne'
GROUP BY (
    amenageur,
    nom_commune
)
ORDER BY
    2,
    3 DESC;
```

## 5.2 Le nombre de prises et la puissance maximale par aménageur

```sql
SELECT
    amenageur,
    SUM(nb_charges) nbcharges,
    MAX(puissance_max) puissancemax
FROM
    station
    NATURAL JOIN i2b03a.borne
GROUP BY (
    amenageur
)
ORDER BY
    2 DESC;
```

### 5.3 Répartition des prises par communes par département


Pour rendre les requêtes plus simples, nous avons créé la vue `total` sur le profil de Camille. Elle représente le nombre de prises total par département et permet ensuite de calculer un ratio. Il a aussi fallu donner aux autres membres du groupe le droit de lire la vue.

```sql
CREATE VIEW total AS
    SELECT
        SUM(nb_charges) tot,
        nom_dep
    FROM
        i2b03a.borne
        NATURAL JOIN station
        NATURAL JOIN i2b01a.commune
        NATURAL JOIN i2b01a.departement
    GROUP BY
        nom_dep;

GRANT SELECT ON total TO i2b01a;

GRANT SELECT ON total TO i2b03a;

GRANT SELECT ON total TO i2b07b;
```

### 1. Loire-Atlantique (Louis)

```sql
SELECT
    amenageur,
     SUM(nb_charges) / (SELECT tot FROM i2b01b.total t WHERE t.nom_dep = d.nom_dep) "Ratio Loire-Atlantique"
FROM
    borne
    NATURAL JOIN i2b01b.station
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01a.departement d
WHERE d.nom_dep = 'Loire-Atlantique'
GROUP BY(amenageur, d.nom_dep);

```

### Sarthe (Enzo)
```sql
SELECT
    amenageur,
    SUM(nb_charges) / (
        SELECT
            tot
        FROM
            i2b01b.total t
        WHERE
            t.nom_dep = d.nom_dep
    ) "Ratio Sarthe"
FROM
    i2b03a.borne
    NATURAL JOIN i2b01b.station
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01a.departement d
WHERE
    d.nom_dep = 'Sarthe'
GROUP BY (
    amenageur,
    d.nom_dep
);
```

![alt](img/sarthe1.png)

### Vendée (Lomann)

```sql
SELECT
    amenageur,
     SUM(nb_charges) / (SELECT tot FROM i2b01b.total t WHERE t.nom_dep = d.nom_dep) "Vendée"
FROM
    I2B03A.borne
    NATURAL JOIN i2b01b.station
    NATURAL JOIN commune
    NATURAL JOIN departement d
WHERE d.nom_dep = 'Vendée'
GROUP BY(amenageur, d.nom_dep);
```

![alt](img/vendee1.png)


### Mayenne (Camille)

```sql
SELECT
    amenageur,
     SUM(nb_charges) / (SELECT tot FROM i2b01b.total t WHERE t.nom_dep = d.nom_dep) "Ratio Mayenne"
FROM
    i2b03a.borne
    NATURAL JOIN station
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01a.departement d
WHERE d.nom_dep = 'Mayenne'
GROUP BY(amenageur, d.nom_dep);
```

![alt](img/mayenne1.png)

## Proposez deux autres requêtes

### Liste des opérateurs ayant une borne avec au moins trois prises dessus

```sql
SELECT DISTINCT
    operateur
FROM
    station
    NATURAL JOIN i2b01a.departement   d
    NATURAL JOIN i2b03a.borne         b
WHERE
    d.nom_dep = 'Mayenne'
    AND b.nb_charges > 3
ORDER BY
    1;
```

### Le nombre de prises de chaque type par opérateur

```sql
SELECT
    type_prise, operateur, sum(nb_charges)
FROM
    i2b03a.borne NATURAL JOIN i2b01b.station
GROUP BY
    (operateur, type_prise)
ORDER BY 3 DESC, 2;
```

![alt](img/exo_debile.png)


## 6. Créer une vue globale des Pays de la Loire (Enzo)

```sql
CREATE VIEW paysdelaloire AS
SELECT  
    *
FROM
    region
    NATURAL JOIN i2b01a.departement
    NATURAL JOIN i2b01a.commune
    NATURAL JOIN i2b01b.station
    NATURAL JOIN i2b03a.borne;
```

Les droits sont ensuite accordés avec

```sql
GRANT SELECT ON paysdelaloire TO i2b01a;
GRANT SELECT ON paysdelaloire TO i2b03a;
GRANT SELECT ON paysdelaloire TO i2b01b;
```

Cela fonctionne car la permission `GRANT OPTION` a été accordée lors de la création des tables.
