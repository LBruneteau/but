CREATE TABLE rech
    AS
        SELECT
            *
        FROM
            basetd.rechargeelectrique;
            
CREATE TABLE region (
    code_reg   NUMBER(2, 0),
    nom_reg    VARCHAR2(50),
    PRIMARY KEY ( code_reg )
);

INSERT INTO region VALUES (
    52,
    'Pays de la Loire'
);

CREATE TABLE departement (
    code_dep   NUMBER(2, 0) PRIMARY KEY,
    nom_dep    VARCHAR2(50),
    code_reg   NUMBER(2, 0),
    FOREIGN KEY ( code_reg )
        REFERENCES region ( code_reg )
);

INSERT INTO departement VALUES (
    44,
    'Loire-Atlantique',
    52
);

INSERT INTO departement VALUES (
    49,
    'Maine-et-Loire',
    52
);

INSERT INTO departement VALUES (
    53,
    'Mayenne',
    52
);

INSERT INTO departement VALUES (
    72,
    'Sarthe',
    52
);

INSERT INTO departement VALUES (
    85,
    'Vendee',
    52
);

UPDATE departement
SET
    nom_dep = 'Vendée'
WHERE
    code_dep = 85; -- TODO être intelligent et savoir se relire.

CREATE TABLE commune
    AS
        SELECT DISTINCT
            to_number(substr(insee, 1, 5)) code_insee,
            commune nom_commune,
            code_dep
        FROM
            rech;
            
ALTER TABLE commune ADD CONSTRAINT cp_commune PRIMARY KEY(code_insee);
