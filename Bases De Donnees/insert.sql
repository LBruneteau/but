INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (20, 'Marcel', 35, 2000, 3);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (23, 'Claude', 20, 2500, 3);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (37, 'Michèle', 35, 3000, 3);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (39, 'Léon', 35, 1900, 1);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (41, 'Jules', 35, 2800, 1);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (30, 'Edith', 30, 4000, 4);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (17, 'Sophie', 35, 2800, 2);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (57, 'Anne', 35, 1300, 2);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (68, 'Casimir', 20, 3000, 4);
INSERT INTO employe(nuempl, nomempl, hebdo, salaire, affect) VALUES (10, 'Martin', 20, 1000, 4);

INSERT INTO service(nuserv, nomserv, chef) VALUES (1, 'achat', 41);
INSERT INTO service(nuserv, nomserv, chef) VALUES (2, 'vente', 17);
INSERT INTO service(nuserv, nomserv, chef) VALUES (3, 'informatique', 23);
INSERT INTO service(nuserv, nomserv, chef) VALUES (4, 'mécanique', 20);

INSERT INTO projet(nuproj, nomproj, chef) VALUES (1, 'Cobra', 20);
INSERT INTO projet(nuproj, nomproj, chef) VALUES (2, 'Zorro', 20);
INSERT INTO projet(nuproj, nomproj, chef) VALUES (3, 'Erasmus', 57);
INSERT INTO projet(nuproj, nomproj, chef) VALUES (4, 'Commet', 20);
INSERT INTO projet(nuproj, nomproj, chef) VALUES (5, 'Eureka', 57);
INSERT INTO projet(nuproj, nomproj, chef) VALUES (6, 'Esprit', 17);
