CREATE TABLE departement
    AS
        SELECT
            *
        FROM
            basetd.departement;

CREATE TABLE commune
    AS
        SELECT
            *
        FROM
            basetd.commune;

CREATE TABLE operateur
    AS
        SELECT
            *
        FROM
            basetd.operateur;

CREATE TABLE distribution
    AS
        SELECT
            *
        FROM
            basetd.distribution;

CREATE VIEW vue_1 AS
    SELECT
        co.nom_commune,
        co.code_insee,
        op.nomfo,
        op.generation,
        op.technologie,
        de.nomdep,
        de.code_departement,
        di.adresse
    FROM
        commune        co,
        departement    de,
        operateur      op,
        distribution   di
    WHERE
        op.numfo = di.numfo
        AND co.code_insee = di.code_insee
        AND co.nomdep = de.nomdep
        AND generation = '5G'
        AND co.nom_commune = 'Carquefou';

SELECT
    nomfo,
    COUNT(*)
FROM
    vue_1
GROUP BY
    nomfo;

CREATE VIEW top10 AS
    SELECT
        co.nom_commune,
        COUNT(op.generation)
    FROM
        commune        co,
        departement    de,
        operateur      op,
        distribution   di
    WHERE
        op.numfo = di.numfo
        AND co.code_insee = di.code_insee
        AND co.nomdep = de.nomdep
        AND generation = '5G'
    GROUP BY
        co.nom_commune
    ORDER BY
        2 DESC,
        1
    FETCH NEXT 10 ROWS ONLY;

SELECT
    *
FROM
    top10
ORDER BY
    2;

CREATE VIEW r1 AS
    SELECT
        co.code_insee,
        co.nom_commune,
        (
            SELECT
                COUNT(*)
            FROM
                distribution   di,
                operateur      op
            WHERE
                op.numfo = di.numfo
                AND co.code_insee = di.code_insee
                AND op.generation = '5G'
        ) nb5g
    FROM
        commune co
    WHERE
        co.nomdep = 'Loire-Atlantique'
    ORDER BY
        2 DESC;

CREATE VIEW r2 AS
    SELECT
        co.code_insee,
        co.nom_commune,
        (
            SELECT
                COUNT(*)
            FROM
                distribution   di,
                operateur      op
            WHERE
                op.numfo = di.numfo
                AND co.code_insee = di.code_insee
                AND op.generation = '4G'
        ) nb4g
    FROM
        commune co
    WHERE
        co.nomdep = 'Loire-Atlantique'
    ORDER BY
        2 DESC;

SELECT
    *
FROM
    r1
    NATURAL JOIN r2
ORDER BY
    3 DESC
FETCH NEXT 10 ROWS ONLY;

INSERT INTO r1 VALUES (
    44999,
    'fontome',
    200
); -- marche pas

DROP TABLE distribution;

DROP TABLE operateur;

DROP TABLE commune;

DROP TABLE departement;

CREATE TABLE departementla
    AS
        SELECT
            *
        FROM
            basetd.departement
        WHERE
            code_departement = 44;

CREATE TABLE communela
    AS
        SELECT
            *
        FROM
            basetd.commune
        WHERE
            nomdep IN (
                SELECT
                    nomdep
                FROM
                    departementla
                WHERE
                    code_departement = 44
            );

CREATE TABLE distributionla
    AS
        SELECT
            *
        FROM
            basetd.distribution
        WHERE
            code_insee IN (
                SELECT
                    code_insee
                FROM
                    communela
            );

CREATE TABLE operateurla
    AS
        SELECT
            *
        FROM
            basetd.operateur
        WHERE
            numfo IN (
                SELECT
                    numfo
                FROM
                    distributionla
            );

ALTER TABLE departementla ADD CONSTRAINT p_dep PRIMARY KEY ( nomdep );

ALTER TABLE communela ADD CONSTRAINT p_com PRIMARY KEY ( code_insee );

ALTER TABLE operateurla ADD CONSTRAINT p_op PRIMARY KEY ( numfo );

ALTER TABLE distributionla ADD CONSTRAINT p_dist PRIMARY KEY ( id );

ALTER TABLE communela
    ADD CONSTRAINT f_com_dep FOREIGN KEY ( nomdep )
        REFERENCES departementla ( nomdep );

ALTER TABLE distributionla
    ADD CONSTRAINT f_com_dist FOREIGN KEY ( code_insee )
        REFERENCES communela ( code_insee );

ALTER TABLE distributionla
    ADD CONSTRAINT f_op_dist FOREIGN KEY ( numfo )
        REFERENCES operateurla ( numfo );

INSERT INTO communela VALUES (
    44990,
    'fontome',
    'Loire-Atlantique'
);

DELETE FROM operateurla
WHERE
    generation IN (
        '2G',
        '3G'
    );

SELECT
    *
FROM
    operateurla
    NATURAL JOIN distributionla;

ALTER TABLE operateurla ADD antq
    NUMBER ( 4, 0 );
ALTER TABLE operateurla ADD antc
    NUMBER ( 4, 0 );
    
CREATE VIEW c AS
    SELECT
        co.code_insee,
        co.nom_commune,
        (
            SELECT
                COUNT(*)
            FROM
                distributionla   di,
                operateurla     op
            WHERE
                op.numfo = di.numfo
                AND co.code_insee = di.code_insee
                AND op.generation = '5G'
        ) nb5g
    FROM
        communela co
    WHERE
        co.nomdep = 'Loire-Atlantique';
        
CREATE VIEW q AS
    SELECT
        co.code_insee,
        co.nom_commune,
        (
            SELECT
                COUNT(*)
            FROM
                distributionla   di,
                operateurla     op
            WHERE
                op.numfo = di.numfo
                AND co.code_insee = di.code_insee
                AND op.generation = '4G'
        ) nb5g
    FROM
        communela co
    WHERE
        co.nomdep = 'Loire-Atlantique';
