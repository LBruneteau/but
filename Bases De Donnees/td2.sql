SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    nomdep IN (
        SELECT
            nomdep
        FROM
            basetd.departement
        WHERE
            code_departement = 49
    )
ORDER BY
    1;

SELECT
    nom_commune
FROM
    basetd.commune c
WHERE
    EXISTS (
        SELECT
            *
        FROM
            basetd.departement d
        WHERE
            code_departement = 49
            AND c.nomdep = d.nomdep
    )
ORDER BY
    1;
    
-- 2

SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune        c
    INNER JOIN basetd.departement    d ON c.nomdep = d.nomdep
    INNER JOIN basetd.distribution   di ON di.code_insee = c.code_insee
    INNER JOIN basetd.operateur      o ON o.numfo = di.numfo
WHERE
    generation = '5G';

SELECT DISTINCT
    c.nom_commune,
    c.nomdep
FROM
    basetd.commune c
WHERE
    c.code_insee IN (
        SELECT
            code_insee
        FROM
            basetd.distribution d
        WHERE
            d.numfo IN (
                SELECT
                    numfo
                FROM
                    basetd.operateur o
                WHERE
                    o.numfo = d.numfo
                    AND generation = '5G'
            )
    )
ORDER BY
    1;

SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    EXISTS (
        SELECT
            *
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
            AND di.code_insee = c.code_insee
    )
ORDER BY
    1;
    
-- 3

SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    c.code_insee NOT IN (
        SELECT
            code_insee
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
    )
ORDER BY
    1;

SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
            AND di.code_insee = c.code_insee
    )
ORDER BY
    1;

-- 4

SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    c.code_insee NOT IN (
        SELECT
            code_insee
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
    )
    AND d.code_departement = 44
ORDER BY
    1;

SELECT DISTINCT
    c.nom_commune,
    d.nomdep
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            basetd.operateur      o
            INNER JOIN basetd.distribution   di ON di.numfo = o.numfo
        WHERE
            generation = '5G'
            AND di.code_insee = c.code_insee
    )
        AND d.code_departement = 44
ORDER BY
    1;

-- 5

SELECT
    d.nomdep,
    COUNT(*)
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep
GROUP BY
    d.nomdep
ORDER BY
    1;

SELECT
    d.nomdep,
    (
        SELECT
            COUNT(*)
        FROM
            basetd.commune c
        WHERE
            c.nomdep = d.nomdep
    ) compte
FROM
    basetd.departement d
ORDER BY
    1;

SELECT
    c.nom_commune,
    COUNT(*)
FROM
    basetd.commune        c
    INNER JOIN basetd.distribution   di ON c.code_insee = di.code_insee
    INNER JOIN basetd.operateur      o ON o.numfo = di.numfo
WHERE
    generation = '5G'
    AND nomdep = 'Loire-Atlantique'
GROUP BY
    c.nom_commune
ORDER BY
    COUNT(*) DESC;

SELECT
    nom_commune,
    code_departement
FROM
    basetd.commune       c
    INNER JOIN basetd.departement   d ON c.nomdep = d.nomdep;
    
-- 6 ------------------------------------------
-- 1

SELECT
    c.nom_commune,
    COUNT(*)
FROM
    basetd.commune        c
    INNER JOIN basetd.distribution   d ON d.code_insee = c.code_insee
    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
WHERE
    generation = '5G'
    AND nomdep = 'Loire-Atlantique'
GROUP BY (
    c.nom_commune
)
ORDER BY
    2,
    1;

-- 2

SELECT
    *
FROM
    (
        SELECT
            c.nom_commune,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '5G'
            ) compte
        FROM
            basetd.commune c
        WHERE
            nomdep = 'Loire-Atlantique'
    )
WHERE
    compte > 0
ORDER BY
    2,
    1;
    
    
------------------- 7

SELECT
    c.nom_commune,
    c.nomdep,
    COUNT(*)
FROM
    basetd.commune        c
    INNER JOIN basetd.distribution   d ON d.code_insee = c.code_insee
    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
WHERE
    generation = '5G'
GROUP BY (
    c.nom_commune,
    c.nomdep
)
HAVING ( COUNT(*) >= 10 )
ORDER BY
    3,
    1;
    
--- 2

SELECT
    *
FROM
    (
        SELECT
            c.nom_commune,
            c.nomdep,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '5G'
            ) compte
        FROM
            basetd.commune c
    )
WHERE
    compte >= 10
ORDER BY
    3,
    1;
    
---------- 8

SELECT
    *
FROM
    (
        SELECT
            c.nom_commune,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '5G'
            ) "5G",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution   d
                    INNER JOIN basetd.operateur      o ON o.numfo = d.numfo
                WHERE
                    d.code_insee = c.code_insee
                    AND generation = '4G'
            ) "4G"
        FROM
            basetd.commune c
        WHERE
            c.nomdep = 'Loire-Atlantique'
    )
ORDER BY
    2 DESC,
    3 DESC,
    1;
    
----------------- 9

SELECT
    nomfo,
    technologie,
    COUNT(*) nombre
FROM
    basetd.operateur
    NATURAL JOIN basetd.distribution
WHERE
    technologie IN (
        '5G NR 3500',
        '5G NR 2100',
        '5G NR 700'
    )
GROUP BY (
    nomfo,
    technologie
);

-- 2

SELECT
    nomfo,
    MAX("3500") "3500",
    MAX("2100") "2100",
    MAX("700") "700"
FROM
    (
        SELECT DISTINCT
            o.nomfo,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 3500'
                    AND o.numfo = d.numfo
            ) "3500",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 2100'
                    AND o.numfo = d.numfo
            ) "2100",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 700'
                    AND o.numfo = d.numfo
            ) "700"
        FROM
            basetd.operateur o
    )
GROUP BY (
    nomfo
);

-- 10 ------------------

SELECT
    nomfo,
    technologie,
    COUNT(*) nombre
FROM
    basetd.operateur
    NATURAL JOIN basetd.distribution
    NATURAL JOIN basetd.commune
WHERE
    technologie IN (
        '5G NR 3500',
        '5G NR 2100',
        '5G NR 700'
    )
    AND nom_commune = 'Nantes'
GROUP BY (
    nomfo,
    technologie
);

-- 2

SELECT
    nomfo,
    MAX("3500") "3500",
    MAX("2100") "2100",
    MAX("700") "700"
FROM
    (
        SELECT DISTINCT
            o.nomfo,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 3500'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "3500",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 2100'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "2100",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 700'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "700"
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            nom_commune = 'Nantes'
    )
GROUP BY (
    nomfo
);

-- 11 --------------------

SELECT
    nomfo,
    technologie,
    COUNT(*) nombre
FROM
    basetd.operateur
    NATURAL JOIN basetd.distribution
    NATURAL JOIN basetd.commune
WHERE
    technologie IN (
        '5G NR 3500',
        '5G NR 2100',
        '5G NR 700'
    )
    AND nomdep = 'Loire-Atlantique'
GROUP BY (
    nomfo,
    technologie
);

-- 2

SELECT
    nomfo,
    MAX("3500") "3500",
    MAX("2100") "2100",
    MAX("700") "700"
FROM
    (
        SELECT DISTINCT
            o.nomfo,
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 3500'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "3500",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 2100'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "2100",
            (
                SELECT
                    COUNT(*)
                FROM
                    basetd.distribution d
                WHERE
                    o.technologie = '5G NR 700'
                    AND o.numfo = d.numfo
                    AND c.code_insee = d.code_insee
            ) "700"
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.nomdep = 'Loire-Atlantique'
    )
GROUP BY (
    nomfo
);
-------- 12

SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    code_insee NOT IN (
        SELECT
            c.code_insee
        FROM
            basetd.operateur,
            basetd.commune c
        WHERE
            numfo NOT IN (
                SELECT DISTINCT
                    numfo
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
            )
    );
    
    -- 2

SELECT
    nom_commune
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            c.code_insee
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.code_insee = co.code_insee
            AND NOT EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
                    AND o.numfo = numfo
            )
    );
------------ 13

SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    code_insee NOT IN (
        SELECT
            c.code_insee
        FROM
            basetd.operateur,
            basetd.commune c
        WHERE
            generation = '5G'
            AND numfo NOT IN (
                SELECT DISTINCT
                    numfo
                FROM
                    basetd.operateur      op
                    NATURAL JOIN basetd.distribution   d
                WHERE
                    d.code_insee = c.code_insee
                    AND op.generation = '5G'
            )
    );
-- 2

SELECT
    nom_commune
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            c.code_insee
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.code_insee = co.code_insee
            AND o.generation = '5G'
            AND NOT EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
                    AND o.numfo = numfo
                    AND generation = '5G'
            )
    );
------------- 14
SELECT
    nom_commune
FROM
    basetd.commune
WHERE
    code_insee NOT IN (
        SELECT
            c.code_insee
        FROM
            basetd.operateur,
            basetd.commune c
        WHERE
            generation = '4G' AND nomfo = 'ORANGE'
            AND numfo NOT IN (
                SELECT DISTINCT
                    numfo
                FROM
                    basetd.operateur      op
                    NATURAL JOIN basetd.distribution   d
                WHERE
                    d.code_insee = c.code_insee
                    AND op.generation = '4G' AND op.nomfo = 'ORANGE'
            )
    );
    
--  2
SELECT
    nom_commune
FROM
    basetd.commune co
WHERE
    NOT EXISTS (
        SELECT
            c.code_insee
        FROM
            basetd.operateur   o,
            basetd.commune     c
        WHERE
            c.code_insee = co.code_insee
            AND o.generation = '4G' AND o.nomfo = 'ORANGE'
            AND NOT EXISTS (
                SELECT
                    *
                FROM
                    basetd.operateur
                    NATURAL JOIN basetd.distribution d
                WHERE
                    d.code_insee = c.code_insee
                    AND o.numfo = numfo
                    AND generation = '4G' AND nomfo = 'ORANGE'
            )
    );