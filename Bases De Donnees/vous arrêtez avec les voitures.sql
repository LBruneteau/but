SELECT COUNT(*) FROM basetd.commune;

SELECT COUNT(*) FROM basetd.departement;

SELECT COUNT(*) FROM basetd.commune, basetd.departement order by nom_commune;

SELECT c.nom_commune, d.code_departement FROM basetd.commune c INNER JOIN basetd.departement d ON c.nomdep = d.nomdep ORDER BY nom_commune;

SELECT nom_commune, c.code_insee, o.nomfo, generation, technologie, code_departement, adresse FROM
basetd.commune c INNER JOIN basetd.distribution d ON c.code_insee = d.code_insee
INNER JOIN basetd.departement de ON de.NOMDEP = c.NOMDEP
INNER JOIN basetd.operateur o ON o.numfo = d.numfo
WHERE NOM_COMMUNE = 'Nantes' AND generation = '5G'
ORDER BY 1;  

SELECT adresse, nomfo
FROM basetd.distribution di INNER JOIN basetd.operateur o ON o.numfo = di.numfo 
INNER JOIN basetd.commune c on c.code_insee = di.code_insee
WHERE technologie = '5G NR 3500' AND nom_commune = 'Nantes'
order by 1;

SELECT DISTINCT nom_commune, generation, nomfo 
FROM basetd.commune c 
INNER JOIN basetd.distribution di ON di.code_insee = c.code_insee
INNER JOIN basetd.operateur o ON o.numfo = di.numfo
WHERE generation = '5G'
ORDER BY 1;

SELECT nom_commune, nomfo, COUNT(*)
FROM basetd.commune c INNER JOIN basetd.distribution di on c.code_insee = di.code_insee
INNER JOIN basetd.operateur o ON o.numfo = di.numfo
GROUP BY (nom_commune, nomfo)
ORDER BY COUNT(*) DESC;

SELECT DISTINCT nom_commune, generation
FROM basetd.commune c 
INNER JOIN basetd.distribution di ON di.code_insee = c.code_insee
INNER JOIN basetd.operateur o ON o.numfo = di.numfo
WHERE generation = '5G'
GROUP BY (nom_commune, generation)
ORDER BY COUNT(*) DESC
FETCH NEXT 10 ROWS ONLY;

SELECT nom_commune, nomfo, COUNT(*)
FROM basetd.commune c INNER JOIN basetd.distribution di on c.code_insee = di.code_insee
INNER JOIN basetd.operateur o ON o.numfo = di.numfo
GROUP BY (nom_commune, nomfo) 
HAVING COUNT(*) > 50
ORDER BY COUNT(*) DESC;

SELECT nomdep, nomfo, COUNT(*)
from basetd.commune c 
INNER JOIN basetd.distribution di on c.code_insee = di.code_insee
INNER JOIN basetd.operateur o ON o.numfo = di.numfo
WHERE generation = '4G'
GROUP BY (nomdep, nomfo)
ORDER BY COUNT(*) DESC;