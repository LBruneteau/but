CREATE TABLE employe (
    nuempl NUMBER(4, 0) PRIMARY KEY,
    nomempl VARCHAR2(20) NOT NULL,
    hebdo NUMBER(2, 0) NOT NULL CHECK(hebdo <= 35),
    salaire NUMBER(5, 0),
    affect NUMBER(2, 0)
);

CREATE TABLE service(
    nuserv NUMBER(4, 0) PRIMARY KEY,
    nomserv VARCHAR(20) NOT NULL,
    chef NUMBER(4, 0) NOT NULL,
    CONSTRAINT fk_service_chef FOREIGN KEY (chef) REFERENCES employe(nuempl)
);

CREATE TABLE projet (
    nuproj NUMBER(4, 0) PRIMARY KEY,
    nomproj VARCHAR2(20) NOT NULL,
    chef NUMBER(4, 0) NOT NULL,
    CONSTRAINT fk_projet_chef FOREIGN KEY (chef) REFERENCES employe(nuempl)
);

CREATE TABLE travail(
    nuempl NUMBER(4, 0) NOT NULL,
    nuproj NUMBER(4, 0) NOT NULL,
    duree NUMBER(4, 0) NOT NULL,
    CONSTRAINT fk_travail_nuempl FOREIGN KEY (nuempl) REFERENCES employe(nuempl),
    CONSTRAINT fk_travail_nuproj FOREIGN KEY (nuproj) REFERENCES projet(nuproj),
    CONSTRAINT pk_travail PRIMARY KEY (nuempl, nuproj)
);

CREATE TABLE concerne(
    nuserv NUMBER(4, 0),
    nuproj NUMBER(4, 0),
    CONSTRAINT fk_concerne_nuserv FOREIGN KEY (nuserv) REFERENCES service(nuserv),
    CONSTRAINT fk_concerne_nuproj FOREIGN KEY (nuproj) REFERENCES projet(nuproj),
    CONSTRAINT pk_concerne PRIMARY KEY (nuserv, nuproj)
);