CREATE TABLE communeve
    AS
        SELECT
            *
        FROM
            basetd.commune
        WHERE
            nomdep = 'Vendée';

CREATE TABLE distributionve
    AS
        SELECT
            *
        FROM
            basetd.distribution
        WHERE
            code_insee IN (
                SELECT
                    code_insee
                FROM
                    communeve
            )
            AND numfo IN (
                SELECT
                    numfo
                FROM
                    basetd.operateur
                WHERE
                    generation IN (
                        '5G',
                        '4G'
                    )
            );

CREATE TABLE operateurve
    AS
        SELECT
            *
        FROM
            basetd.operateur o
        WHERE
            EXISTS (
                SELECT
                    *
                FROM
                    distributionve
                WHERE
                    numfo = o.numfo
            );

ALTER TABLE communeve ADD CONSTRAINT cp_commune PRIMARY KEY ( code_insee );

ALTER TABLE distributionve ADD CONSTRAINT cp_distribution PRIMARY KEY ( id );

ALTER TABLE operateurve ADD CONSTRAINT cp_operateur PRIMARY KEY ( numfo );

ALTER TABLE distributionve
    ADD CONSTRAINT ce_distribution_commune FOREIGN KEY ( code_insee )
        REFERENCES communeve ( code_insee );

ALTER TABLE distributionve
    ADD CONSTRAINT ce_distribution_operateur FOREIGN KEY ( numfo )
        REFERENCES operateurve ( numfo );

GRANT SELECT ON communeve TO i2b01b;

GRANT SELECT ON distributionve TO i2b01b;

GRANT SELECT ON operateurve TO i2b01b;

GRANT SELECT ON communeve TO i2b01a;

GRANT SELECT ON distributionve TO i2b01a;

GRANT SELECT ON operateurve TO i2b01a;

SELECT
    code_insee,
    COUNT(*)
FROM
    communeve
    NATURAL JOIN distributionve
    NATURAL JOIN operateurve
WHERE
    generation = '4G'
GROUP BY
    code_insee
ORDER BY
    2 DESC;

GRANT UPDATE ( adresse ) ON distributionve TO i2b01a;

GRANT UPDATE ( adresse ) ON distributionve TO i2b01b;

GRANT INSERT ON distributionve TO i2b01a;

GRANT INSERT ON distributionve TO i2b01b;

INSERT INTO i2b03a.distributionve VALUES (
    33,
    7,
    85011,
    'coucou 24 desbisous',
    85630,
    'bisous',
    NULL,
    53.02,
    65.02,
    'coucoudesbisous'
);

COMMIT;

SELECT
    *
FROM
    distributionve
WHERE
    statut = 'bisous';

REVOKE ALL ON distributionve FROM i2b01b;

REVOKE ALL ON distributionve FROM i2b01a;

REVOKE SELECT ON operateurve FROM i2b01b;

REVOKE SELECT ON operateurve FROM i2b01a;

REVOKE SELECT ON communeve FROM i2b01b;

REVOKE SELECT ON communeve FROM i2b01a;

CREATE VIEW minidistve AS
    SELECT
        id,
        numfo,
        code_insee,
        adresse,
        statut
    FROM
        distributionve;

GRANT SELECT ON minidistve TO i2b01a;

GRANT SELECT ON minidistve TO i2b01b;

CREATE VIEW ville5g AS
    SELECT
        code_insee,
        nom_commune,
        COUNT(*) nb5g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '5G'
    GROUP BY (
        code_insee,
        nom_commune
    );

CREATE VIEW ville4g AS
    SELECT
        code_insee,
        nom_commune,
        COUNT(*) nb4g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '4G'
    GROUP BY (
        code_insee,
        nom_commune
    );

GRANT SELECT ON ville4g TO i2b01a;

GRANT SELECT ON ville4g TO i2b01b;

GRANT SELECT ON ville5g TO i2b01a;

GRANT SELECT ON ville5g TO i2b01b;

CREATE VIEW op5g AS
    SELECT
        code_insee,
        nom_commune,
        nomfo,
        COUNT(*) nb5g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '5G'
    GROUP BY (
        code_insee,
        nom_commune,
        nomfo
    );

CREATE VIEW op4g AS
    SELECT
        code_insee,
        nom_commune,
        nomfo,
        COUNT(*) nb4g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '4G'
    GROUP BY (
        code_insee,
        nom_commune,
        nomfo
    );

GRANT SELECT ON op4g TO i2b01a;

GRANT SELECT ON op4g TO i2b01b;

GRANT SELECT ON op5g TO i2b01a;

GRANT SELECT ON op5g TO i2b01b;

CREATE VIEW techno AS
    SELECT
        code_insee,
        nom_commune,
        technologie,
        COUNT(*) nb
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    GROUP BY (
        code_insee,
        nom_commune,
        technologie
    );

GRANT SELECT ON techno TO i2b01a;

GRANT SELECT ON techno TO i2b01b;

CREATE VIEW orange AS SELECT
    *
FROM
    op5g
    NATURAL JOIN op4g
WHERE
    nomfo = 'ORANGE'
ORDER BY
    4 DESC
FETCH NEXT 20 ROWS ONLY;

GRANT SELECT ON orange TO i2b01a;

GRANT SELECT ON orange TO i2b01b;


REVOKE ALL ON distributionve FROM i2b01a;
REVOKE ALL ON communeve FROM i2b01a;
REVOKE ALL ON operateurve FROM i2b01a;

REVOKE ALL ON distributionve FROM i2b01b;
REVOKE ALL ON communeve FROM i2b01b;
REVOKE ALL ON operateurve FROM i2b01b;



CREATE ROLE i2b03a_Mon_Ami;

SET ROLE i2b03a_Mon_Ami;
GRANT SELECT ON communeve TO i2b03a_Mon_Ami;
GRANT SELECT ON distributionve TO i2b03a_Mon_Ami;
GRANT SELECT ON operateurve TO i2b03a_Mon_Ami;

GRANT UPDATE ON distributionve TO i2b03a_Mon_Ami;
GRANT UPDATE ON distributionve TO i2b03a_Mon_Ami;

GRANT INSERT ON distributionve TO i2b03a_Mon_Ami;
GRANT INSERT ON distributionve TO i2b03a_Mon_Ami;
GRANT i2b03a_Mon_Ami TO i2b01b;
GRANT i2b03a_Mon_Ami TO i2b01a;
imposteurovisionnairereurekamionmonduleuberlustucruditélévisionnaireeditaireectionbragéeatiogbéteàcornetdeglacetiquerdetraindeuxtroispetitschatsstetéverticalculatorificeteléphoneiturescolairerdrepliteraturetreounepasetreouetrematernitétonbéstialaouakbananeanasticotidientriguétapantèreoportaucaramelancoliqueornementtiraubutopiepizzaiolographiqueoniqueelephanttomedefromageaccioplaythoreontostralopitèqueuilapinnterimaginetoniceaspirateurdujambonbeurrerekastornithorinqueulterieurementde'amourirauxéclatsfoutisngchongelaversselledeguérandeezmoimongouternorcoréendutoutrelleicopiterriermenagerabiarritziganeacheisdedanstisteanbouleghourderapagedecomtésursesdoigtsternébreuxbeuxtonneuseophagederaisonnartichautcolatfermiereronélataupe;
À lwi