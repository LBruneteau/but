# Ressource R2.062 - Exploitation d'une Base de Données, Privilèges et Rôles

##### Auteur
*Louis BRUNETEAU*

##### Membres du groupe
- *Louis BRUNETEAU* (`i2b03a`)
- *Camille MOREAU* (`i2b01b`)
- *Lomann LECOQ* (`i2b01a`)


## Schéma relationnel de la base de données

Distribution(**ID**, *#NUM_FO*, *#CODE_INSEE*, ADRESSE, STATUT, ...);
Operateur(**NUMFO**, NOMFO, GENERATION, TECHNOLOGIE);
Commune(**CODE_INSEE**, NOMCOMMUNE, *#NOMDEP*);
Departement(**NOMDEP**, CODE_DEPARTEMENT);


## Créer la base de données
Il s'agit de recréer la base de données utilisée dans `basetd`, mais seulement avec les villes de **Vendée**, et seulement les antennes *4G* et *5G*. Les contraintes doivent également être recréées.

### Copie des tables

#### Table Commune

```sql
CREATE TABLE communeve
    AS
        SELECT
            *
        FROM
            basetd.commune
        WHERE
            nomdep = 'Vendée';
```

#### Table Distribution
Cette requête copie la table `basetd.distribution` en ne gardant que les antennes situées en Vendée en vérifiant si le code Insee de l'antenne appartient à l'ensemble des codes Insee de Vendée.
Elle vérifie également si l'antenne dispose de la *4G* ou de la *5G* en faisant une jointure avec la table `basetd.operateur`, afin de ne garder que les antennes de cette génération.
```sql
CREATE TABLE distributionve
    AS
        SELECT
            *
        FROM
            basetd.distribution
        WHERE
            code_insee IN (
                SELECT
                    code_insee
                FROM
                    communeve
            )
            AND numfo IN (
                SELECT
                    numfo
                FROM
                    basetd.operateur
                WHERE
                    generation IN (
                        '5G',
                        '4G'
                    )
            );
```

#### Table Operateur

Cette requête crée la table `operateurve` à partir de la table `basetd.operateur`. Un opérateur est gardé si et seulement s'il existe au moins une antenne dans la table `distributionve` l'utilisant. De cette façon, les seuls opérateurs conservés sont déployés en Vendée, et sont de génération *4G* ou *5G*, car la création de la table `distributionve` l'a vérifié.

```sql
CREATE TABLE operateurve
    AS
        SELECT
            *
        FROM
            basetd.operateur o
        WHERE
            EXISTS (
                SELECT
                    *
                FROM
                    distributionve
                WHERE
                    numfo = o.numfo
            );
```

### Création des clés primaires et étrangères

#### Création des clés primaires

```sql
ALTER TABLE communeve ADD CONSTRAINT cp_commune PRIMARY KEY ( code_insee );

ALTER TABLE distributionve ADD CONSTRAINT cp_distri PRIMARY KEY( id );

ALTER TABLE operateurve ADD CONSTRAINT cp_operateur PRIMARY KEY ( numfo );
```

#### Création des clés étrangères

```sql
ALTER TABLE distributionve
    ADD CONSTRAINT ce_distribution_commune FOREIGN KEY ( code_insee )
        REFERENCES communeve ( code_insee );

ALTER TABLE distributionve
    ADD CONSTRAINT ce_distribution_operateur FOREIGN KEY ( numfo )
        REFERENCES operateurve ( numfo );
```

## Exercice 1 : Droits : Select, insert, update

### 1. Donnez le droit de faire des Select sur vos tables à votre binôme.

```sql
GRANT SELECT ON communeve TO i2b01b;
GRANT SELECT ON distributionve TO i2b01b;
GRANT SELECT ON operateurve TO i2b01b;

GRANT SELECT ON communeve TO i2b01a;
GRANT SELECT ON distributionve TO i2b01a;
GRANT SELECT ON operateurve TO i2b01a;
```

Ces requêtes ont été testées en utilisant `SELECT * FROM i2b03a.communeve;`.

### 2. Donnez le droit de faire un Update du champ Adresse de la table Distribution

Les privilèges ont été accordés en utilisant

```sql
GRANT UPDATE(adresse) ON distributionve TO i2b01a;
GRANT UPDATE(adresse) ON distributionve TO i2b01b;
```

La requête a ensuite été testée en utilisant
```sql
UPDATE i2b03a.distributionve SET adresse='coucou 24 desbisous';
COMMIT;
```

Nous avons ensuite constaté que toutes les adresses de la table `distributionve` ont été remplacées par *coucou 24 desbisous* sur toutes nos sessions.

Cependant, en essayant de modifier un autre attribut que `distributionve.adresse`, un message d'erreur s'est affiché indiquant que les permissions étaient insuffisantes.

```
Erreur commençant à la ligne: 94 de la commande -
UPDATE I2B03A.distributionve SET statut='hors service'
Erreur à la ligne de commande: 94 Colonne: 15
Rapport d'erreur -
Erreur SQL : ORA-01031: privilèges insuffisants
01031. 00000 -  "insufficient privileges"
*Cause:    An attempt was made to perform a database operation without
           the necessary privileges.
*Action:   Ask your database administrator or designated security
           administrator to grant you the necessary privileges
```

### 3. Donnez le droit de faire appel insert à la table `distribution`.
Les commandes utilisées sont

```sql
GRANT INSERT ON distributionve TO i2b01a;
GRANT INSERT ON distributionve TO i2b01b;
```

Ensuite l'insertion a été testée avec
```sql
INSERT INTO i2b03a.distributionve VALUES (
    33,
    7,
    85011,
    'coucou 24 desbisous',
    85630,
    'bisous',
    NULL,
    53.02,
    65.02,
    'coucoudesbisous'
);
COMMIT;
```

puis vérifiée avec

```sql
SELECT
    *
FROM
    distributionve
WHERE
    statut = 'bisous';
```
qui retourne bien la ligne ajoutée.

### 4. Supprimez les différents droits que vous avez donné à la question 1, 2 et 3.

```sql
REVOKE ALL ON distributionve FROM i2b01b;
REVOKE ALL ON distributionve FROM i2b01a;
REVOKE SELECT ON operateurve FROM i2b01b;
REVOKE SELECT ON operateurve FROM i2b01a;
REVOKE SELECT ON communeve FROM i2b01b;
REVOKE SELECT ON communeve FROM i2b01a;
```

Les permissions ont été vérifiées avec les commandes suivantes.

```sql
SELECT * FROM i2b03a.communeve
SELECT * FROM i2b03a.operateurve
SELECT * FROM i2b03a.distributionve

UPDATE i2b03a.distributionve SET adresse='coucou 24 desbisous';

INSERT INTO i2b03a.distributionve VALUES (
    35,
    7,
    85011,
    'coucou 24 desbisous',
    85630,
    'bisous',
    NULL,
    53.02,
    65.02,
    'coucoudesbisous'
);
```

Aucune d'entre elles n'a fonctionné.

### 5. Donnez le droit de voir certains attributs de `distribution`

Il faut créer une vue contenant seulement les attributs de la table `distribution` désirés.
```sql
CREATE VIEW minidistve AS
    SELECT
        id,
        numfo,
        code_insee,
        adresse,
        statut
    FROM
        distributionve;
```

Ensuite, il faut donner la permission d'accéder à ces vues
```sql
GRANT SELECT ON minidistve TO i2b01a;

GRANT SELECT ON minidistve TO i2b01b;
```

La requête
```sql
SELECT * FROM i2b03a.minidistve
```
a ensuite fonctionné, permettant de restreindre certains attributs de la table `distributionve`.

### 6. Donnez le droit de voir le nombre d'antennes 4G et 5G de chaque ville
Les vues sont créées de cette façon

```sql
CREATE VIEW ville5g AS
    SELECT
        code_insee,
        nom_commune,
        COUNT(*) nb5g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '5G'
    GROUP BY (
        code_insee,
        nom_commune
    );

CREATE VIEW ville4g AS
    SELECT
        code_insee,
        nom_commune,
        COUNT(*) nb4g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '4G'
    GROUP BY (
        code_insee,
        nom_commune
    );
```

On peut ensuite donner la permission avec

```sql
GRANT SELECT ON ville4g TO i2b01a;

GRANT SELECT ON ville4g TO i2b01b;

GRANT SELECT ON ville5g TO i2b01a;

GRANT SELECT ON ville5g TO i2b01b;
```

Les tests effectués sont
```sql
SELECT * FROM i2b03a.ville4g;
SELECT * FROM i2b03a.ville5g;
```

### 7. Donnez le droit de voir le nombre d'antennes 4G et 5G de chaque opérateur pour les différentes villes.
La requête est la même que la précédente sauf qu'il faut également regrouper par nom d'opérateur.

```sql
CREATE VIEW op5g AS
    SELECT
        code_insee,
        nom_commune,
        nomfo,
        COUNT(*) nb5g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '5G'
    GROUP BY (
        code_insee,
        nom_commune,
        nomfo
    );

CREATE VIEW op4g AS
    SELECT
        code_insee,
        nom_commune,
        nomfo,
        COUNT(*) nb4g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    WHERE
        generation = '4G'
    GROUP BY (
        code_insee,
        nom_commune,
        nomfo
    );
```

Les privilèges de consultation doivent être accordés
```sql
GRANT SELECT ON op4g TO i2b01a;

GRANT SELECT ON op4g TO i2b01b;

GRANT SELECT ON op5g TO i2b01a;

GRANT SELECT ON op5g TO i2b01b;
```

Ils peuvent le tester avec
```sql
SELECT * FROM i2b03a.op4g;
SELECT * FROM i2b03a.op5g;
```

### 8. Idem avec la répartition des différentes technologies.

Il faut grouper par technologie

```sql
CREATE VIEW techno AS
    SELECT
        code_insee,
        nom_commune,
        technologie,
        COUNT(*) nb
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN operateurve
    GROUP BY (
        code_insee,
        nom_commune,
        technologie
    );
```

Puis donner les permissions
```sql
GRANT SELECT ON techno TO i2b01a;

GRANT SELECT ON techno TO i2b01b;
```

Les tests effectués sont
```sql
SELECT * FROM i2b03a.techno
```

### 9. Donnez les 20 meilleurs villes de l'opérateur orange avec la 5G et la 4G.


```sql
CREATE VIEW orange AS SELECT
    *
FROM
    op5g
    NATURAL JOIN op4g
WHERE
    nomfo = 'ORANGE'
ORDER BY
    4 DESC
FETCH NEXT 20 ROWS ONLY;
```

Il faut ensuite donner les droits

```sql
GRANT SELECT ON orange TO i2b01a;

GRANT SELECT ON orange TO i2b01b;
```

Le test suivant a fonctionné
```sql
SELECT * FROM i2b03a.orange
```

## Exercice 2 : Rôle


### Supprimer les permissions

```sql
REVOKE ALL ON distributionve FROM i2b01a;
REVOKE ALL ON communeve FROM i2b01a;
REVOKE ALL ON operateurve FROM i2b01a;

REVOKE ALL ON distributionve FROM i2b01b;
REVOKE ALL ON communeve FROM i2b01b;
REVOKE ALL ON operateurve FROM i2b01b;
```

### 1. Créez un rôle Mon_Ami.

```sql
CREATE ROLE i2b03a_Mon_Ami;

SET ROLE i2b03a_Mon_Ami;
```

### 2. Affectez à ce rôle les privilèges correspondant aux questions 1,2 et 3.

```sql
GRANT SELECT ON communeve TO i2b03a_Mon_Ami;
GRANT SELECT ON distributionve TO i2b03a_Mon_Ami;
GRANT SELECT ON operateurve TO i2b03a_Mon_Ami;

GRANT UPDATE(adresse) ON distributionve TO i2b03a_Mon_Ami;

GRANT INSERT ON distributionve TO i2b03a_Mon_Ami;
```

### 3. Activez et affectez ce rôle à votre binôme.

```sql
GRANT i2b03a_Mon_Ami TO i2b01b;
GRANT i2b03a_Mon_Ami TO i2b01a;
```

Les tests furent
```sql
SELECT * FROM i2b03a.distributionve;

SELECT * FROM i2b03a.communeve;

SELECT * FROM i2b03a.operateurve;

UPDATE i2b03a.distributionve
SET
    adresse = '69 rue tabaga'
WHERE
    code_insee = 85191;

INSERT INTO i2b03a.distributionve VALUES (
    99,
    18,
    85302,
    'coucou 24 desbisous',
    85000,
    'monmodule',
    '2017-07-28',
    46.6792,
    - 1.4311,
    'coord123'
);
COMMIT;
```

### 4. Supprimer le privilège des select des différentes tables au rôle loginMon_Ami.

```sql
REVOKE SELECT ON communeve FROM i2b03a_Mon_Ami;
REVOKE SELECT ON distributionve FROM i2b03a_Mon_Ami;
REVOKE SELECT ON operateurve FROM i2b03a_Mon_Ami;
```

Après ces requêtes les autres utilisateurs ont un message d'erreur en essayant de faire des `SELECT` sur les différentes tables.

### 5. Supprimer le rôle LoginMon_Ami à votre binôme.

```sql
REVOKE i2b03a_Mon_Ami FROM i2b01b;
REVOKE i2b03a_Mon_Ami FROM i2b01a;
```

La requête suivante échoue car le rôle a été enlevé
```sql
UPDATE i2b03a.distributionve
SET
    adresse = 'Je ne peux rien changer'
WHERE
    code_insee = 85191;
```

### 6. Créez un rôle LoginVoir_mes_tables qui contient les privilèges de la question1 et un autre rôle LoginMAJ_mes_tables avec les privilège de la question 2 et 3.

Les rôles sont créés avec
```sql
CREATE ROLE i2b03a_Voir_mes_tables;
CREATE ROLE i2b03a_MAJ_mes_tables;

SET ROLE i2b03a_Voir_mes_tables;
SET ROLE i2b03a_MAJ_mes_tables;

GRANT SELECT ON communeve TO i2b03a_Voir_mes_tables;
GRANT SELECT ON distributionve TO i2b03a_Voir_mes_tables;
GRANT SELECT ON operateurve TO i2b03a_Voir_mes_tables;

GRANT UPDATE ON distributionve TO i2b03a_MAJ_mes_tables;

GRANT INSERT ON distributionve TO i2b03a_MAJ_mes_tables;
```

### 7. Créez un rôle LoginVoir_Update qui regroupe les rôles de la question précédente.

```sql
CREATE ROLE i2b03a_Voir_Update;

SET ROLE i2b03a_Voir_Update;

GRANT i2b03a_Voir_mes_tables TO i2b03a_Voir_Update ;
GRANT i2b03a_MAJ_mes_tables TO i2b03a_Voir_Update ;
```

### 8. Affectez ce rôle à votre binôme et testez les différentes actions.

```sql
GRANT i2b03a_Voir_Update TO i2b01b;
GRANT i2b03a_Voir_Update TO i2b01a;
```

Cette action a fonctionné, comme en témoignent les tests
```sql
SELECT * FROM i2b03a.distributionve;

SELECT * FROM i2b03a.communeve;

SELECT * FROM i2b03a.operateurve;

UPDATE i2b03a.distributionve
SET
    adresse = '69 rue tabaga'
WHERE
    code_insee = 85191;

INSERT INTO i2b03a.distributionve VALUES (
    99,
    18,
    85302,
    'coucou 24 desbisous',
    85000,
    'monmodule',
    '2017-07-28',
    46.6792,
    - 1.4311,
    'coord123'
);
COMMIT;
```

### 9. Supprimer  le rôle LoginMAJ_mes_tables au rôle LoginVoir_Update

```sql
REVOKE i2b03a_MAJ_mes_tables FROM i2b03a_Voir_Update;
```

Après cette commandes les autres utilisateurs ne peuvent plus modifier les tables. Ainsi la requête
```sql
UPDATE i2b03a.distributionve SET adresse='test';
```
échoue.


## Exercice 3: Distribution et partage des tables

### 1. Utilisateur 1: Créez les tables à partir de basetd qui continent des villes de Loire-Atlantique avec la *4G* ou la *5G*.

Les tables sont créées de la même façon qu'au début du sujet.
```sql
CREATE TABLE communela
    AS
        SELECT
	*
FROM
	basetd.commune co
WHERE
	co.nomdep = 'Loire-Atlantique'
	AND co.code_insee IN (
	SELECT
		CODE_INSEE
	FROM
		BASETD.DISTRIBUTION NATURAL
	JOIN BASETD.OPERATEUR
	WHERE
		generation IN ('4G', '5G') );
```

```sql
CREATE TABLE distributionla
    AS
        SELECT
	*
FROM
	basetd.distribution
WHERE
	code_insee IN (
	SELECT
		code_insee
	FROM
		communela
            );
```

```sql
CREATE TABLE operateurla
    AS
        SELECT
	*
FROM
	basetd.operateur o
WHERE
	EXISTS (
	SELECT
		*
	FROM
		distributionla
	WHERE
		numfo = o.numfo
            );
```

### 2. L’utilisateur 2 :Créez les tables à partir de basetd et de la table OperateurLA de l’utilisateur 1 qui contient les  villes de Vendée avec la 4G et la 5G.

```sql
CREATE TABLE communeve
    AS
        SELECT
            *
        FROM
            basetd.commune
        WHERE
            nomdep = 'Vendée';
```

Pour créer la table `distributionve`, il faut avoir la permission de faire des `SELECT` sur la table `operateurla`.

```sql
GRANT SELECT ON operateurla TO i2b01a;
GRANT SELECT ON operateurla TO i2b01b;
```

La table peut ensuite être créée.

```sql
CREATE TABLE distributionve
    AS
        SELECT
            *
        FROM
            basetd.distribution
        WHERE
            code_insee IN (
                SELECT
                    code_insee
                FROM
                    communeve
            )
            AND numfo IN (
                SELECT
                    numfo
                FROM
                    i2b03a.operateurla
            );
```

### 3. Vous devez maintenant créer les contraintes d’intégrité PK et FK des tables en locale.

#### Utilisateur 1
```sql
ALTER TABLE COMMUNELA ADD CONSTRAINT cp_communela PRIMARY KEY(code_insee);
ALTER TABLE DISTRIBUTIONLA ADD CONSTRAINT cp_distributionla PRIMARY KEY(id);
ALTER TABLE OPERATEURLA ADD CONSTRAINT cp_operateurla PRIMARY KEY(numfo);

ALTER TABLE DISTRIBUTIONLA ADD CONSTRAINT ce_distributionla_operateurla
FOREIGN KEY(numfo) REFERENCES OPERATEURLA(numfo);

ALTER TABLE DISTRIBUTIONLA ADD CONSTRAINT ce_distributionla_communela
FOREIGN KEY(code_insee) REFERENCES COMMUNELA(code_insee);
```

#### Utilisateur 2

```sql
ALTER TABLE COMMUNEVE ADD CONSTRAINT cp_communeve PRIMARY KEY(code_insee);
ALTER TABLE DISTRIBUTIONVE ADD CONSTRAINT cp_distributionve PRIMARY KEY(id);

ALTER TABLE DISTRIBUTIONVE ADD CONSTRAINT ce_distributionve_communeve
FOREIGN KEY(code_insee) REFERENCES COMMUNEVE(code_insee);
```

### 4. Créez maintenant les contraintes distantes.

Tout d'abord l'utilisateur 1 doit nous donner le droit de référencer ses tables.

```sql
GRANT REFERENCES ON OPERATEURLA TO i2b01a;
GRANT REFERENCES ON OPERATEURLA TO i2b01b;
```

Ensuite la clé étrangère peut être créée.
```sql
ALTER TABLE DISTRIBUTIONVE ADD CONSTRAINT ce_distributionve_operateurla
FOREIGN KEY(numfo) REFERENCES I2B03A.OPERATEURLA(numfo);
```

### 5. Refaire les questions 6, 7 et 8 de l'exercice 1.

Tout d'abord l'utilisateur 1 doit donner la permission de faire des `SELECT` sur la table `OPERATEURLA`, ainsi que celle de de redistribuer l'autorisation.

```sql
GRANT SELECT ON operateurla TO i2b01b WITH GRANT OPTION;
GRANT SELECT ON operateurla TO i2b01a WITH GRANT OPTION;
```

#### 6. Nombres d'antennes *4G* et *5G*

L'utilisateur 2 peut créer des vues référençant les tables de l'utilisateur 1.

```sql
CREATE VIEW ville5gve AS
    SELECT
        code_insee,
        nom_commune,
        COUNT(*) nb5g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN i2b03a.operateurla
    WHERE
        generation = '5G'
    GROUP BY (
        code_insee,
        nom_commune
    );
```

```sql
CREATE VIEW ville4gve AS
    SELECT
        code_insee,
        nom_commune,
        COUNT(*) nb4g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN i2b03a.operateurla
    WHERE
        generation = '4G'
    GROUP BY (
        code_insee,
        nom_commune
    );
```

Il peut ensuite donner la permission de faire des `SELECT` sur les tables.

```sql
GRANT SELECT ON VILLE4GVE TO i2b03a;
GRANT SELECT ON VILLE4GVE TO i2b01a;

GRANT SELECT ON VILLE5GVE TO i2b03a;
GRANT SELECT ON VILLE5GVE TO i2b01a;
```

On peut voir que l'opération a fonctionné avec

```sql
SELECT * FROM I2B01B.VILLE4GVE;
SELECT * FROM I2B01B.VILLE5GVE;
```

#### 7. Nombre d'antennes 4G et 5G par opérateur.

La requête est similaire à celle de l'exercice 1

```sql
CREATE VIEW op5gve AS
    SELECT
        code_insee,
        nom_commune,
        nomfo,
        COUNT(*) nb5g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN i2b03a.operateurla
    WHERE
        generation = '5G'
    GROUP BY (
        code_insee,
        nom_commune,
        nomfo
    );
```

```sql
CREATE VIEW op4gve AS
    SELECT
        code_insee,
        nom_commune,
        nomfo,
        COUNT(*) nb4g
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN i2b03a.operateurla
    WHERE
        generation = '4G'
    GROUP BY (
        code_insee,
        nom_commune,
        nomfo
    );
```
On peut donner les permissions aux autres utilisateurs

```sql
GRANT SELECT ON OP4GVE TO i2b03a;
GRANT SELECT ON OP4GVE TO i2b01a;

GRANT SELECT ON OP5GVE TO i2b03a;
GRANT SELECT ON OP5GVE TO i2b01a;
```

Les données sont accessibles via
```sql
SELECT * FROM i2b01b.op4gve;
SELECT * FROM i2b01b.op5gve;
```

#### 8.Répartition des différentes technologies

```sql
CREATE VIEW technove AS
    SELECT
        code_insee,
        nom_commune,
        technologie,
        COUNT(*) nb
    FROM
        communeve
        NATURAL JOIN distributionve
        NATURAL JOIN i2b03a.operateurla
    GROUP BY (
        code_insee,
        nom_commune,
        technologie
    );
```
Les permissions sont données avec
```sql
GRANT SELECT ON technove TO i2b03a;
GRANT SELECT ON technove TO i2b01a;
```

La vue devient ensuite accessible
```sql
SELECT * FROM i2b01b.TECHNOVE;
```
