import pandas as pd
import matplotlib.pyplot as plt

datablblbl = pd.read_csv('pyramide_ages.csv', sep=',')
datableu = datablblbl.copy()


def affichages():
    # Affiche la liste des noms de colonnes
    print(f'data.columns : {datablblbl.columns}')
    print(f'data["age"] : {datablblbl.age}')  # Tous les âges de 1 à 100
    # Toutes les lignes à moins de 60 ans
    print(f'data[data["age"] < 60] : {datablblbl[datablblbl.age < 60]}')


def pyramide_ages_1(ax, annee: str):
    ax.barh(y=datablblbl['age'], width=datablblbl[f'{annee}_F'],
            height=0.5, label='Femmes')
    ax.barh(y=datablblbl['age'], width=-
            datablblbl[f'{annee}_H'], height=0.5, label='Hommes')

    dif = datablblbl[f'{annee}_F'] - datablblbl[f'{annee}_H']
    ax.plot(dif, datablblbl['age'], color='green', label='Différence')

    ax.set(ylabel='Âge', xlabel='Pop')
    ax.set_title(f'Pyramide des âges en {annee}')
    plt.legend(loc='upper right')


def pyramide():
    fig, ax = plt.subplots(1, 3)
    pyramide_ages_1(ax[0], '1975')
    pyramide_ages_1(ax[1], '1999')
    pyramide_ages_1(ax[2], '2019')
    plt.show()


def main():
    affichages()
    pyramide()  # TODO de la purée


if __name__ == '__main__':
    main()
