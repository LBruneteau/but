import matplotlib.pyplot as plt
import TP1_mobilite_sociale as donnees


def exemple():
    fig, ax = plt.subplots()
    ax.barh(y='A', width=10, left=0, height=0.5)
    ax.barh(y='A', width=2, left=2, height=0.5)
    ax.barh(y='B', width=1, left=0, height=0.5)
    ax.barh(y='B', width=4, left=1, height=0.5)

    ax.set_ylim(-1, 1)
    ax.xaxis.set_visible(False)
    plt.show()


def graphique_1():
    _, ax = plt.subplots()
    ag = "Agriculteurs exploitants"
    ax.barh(width=donnees.resultsHomme[ag][0],
            color=donnees.category_colors[0], left=0, height=0.5, y='Agriculteur')
    for i, nom in enumerate(donnees.category_names):
        if not i:
            continue  # Les agriculteurs sont déjà faits.
        gauche = sum(donnees.resultsHomme[ag][:i])
        largeur = donnees.resultsHomme[ag][i]
        ax.barh(width=largeur, color=donnees.category_colors[i],
                left=gauche, height=0.5, y='Agriculteur', label=nom)
    ax.set_ylim(-1, 1)
    ax.xaxis.set_visible(False)
    plt.title(
        'Catégorie socioprofessionnelle\n des hommes ayant un père agriculteur', multialignment='center')
    plt.legend(bbox_to_anchor=(1, 1), loc='best')
    plt.tight_layout()
    plt.show()


def graphque_principal():
    _, ax = plt.subplots(2, 1)
    for i, nom in enumerate(donnees.category_names):
        largeur = [donnees.resultsHomme[j][i] for j in donnees.category_names]
        gauche = [sum(donnees.resultsHomme[j][:i])
                  for j in donnees.category_names]
        ax[0].barh(width=largeur,
                   color=donnees.category_colors[i], left=gauche, height=0.5, y=donnees.category_names, label=nom)
    ax[0].set_title('CSP des hommes en fonction de la CSP de leur père')
    ax[1].xaxis.set_visible(False)
    for i, nom in enumerate(donnees.category_names):
        largeur = [donnees.resultsFemme[j][i] for j in donnees.category_names]
        gauche = [sum(donnees.resultsFemme[j][:i])
                  for j in donnees.category_names]
        print(largeur)
        ax[1].barh(width=largeur,
                   color=donnees.category_colors[i], left=gauche, height=0.5, y=donnees.category_names, label=nom)
    ax[1].set_title('CSP des femmes en fonction de la CSP de leur père')
    ax[1].xaxis.set_visible(False)
    plt.legend(bbox_to_anchor=(1, 1), loc='best')
    plt.tight_layout()
    plt.show()


def main():
    # exemple()
    graphique_1()
    graphque_principal()


if __name__ == '__main__':
    main()
