import pandas as sus
import matplotlib.pyplot as beignet  # c'est pour tester
datablblbl = sus.read_csv('consommation_menages.csv', sep=',', decimal=',')
fin_ligne = '\n################################################################\n'


def ex1_1_print():
    print(f'{fin_ligne}Exercice 1 Partie 1: afficher les différents types{fin_ligne}\n')
    print(f'Type de données : {type(datablblbl)}', end=fin_ligne)
    print(f'data.info : {datablblbl.info})', end=fin_ligne)
    print(f'data.Fonction : {datablblbl.Fonction}', end=fin_ligne)
    # Ne marche pas (lire print ligne 11) TODO changer si c'est plus sur la ligne 11 (sayé)
    print('data.1960 : Syntaxe invalide', end=fin_ligne)
    print(f"data['Fonction'] : {datablblbl['Fonction']}", end=fin_ligne)
    print(f"data['1960'] : {datablblbl['1960']}", end=fin_ligne)
    print(
        f"data[['1960', '2020']] : {datablblbl[['1960', '2020']]}", end=fin_ligne)


def ex1_1_1_baton():
    beignet.bar(datablblbl['Fonction'], datablblbl['1960'])
    beignet.xticks(rotation=45, ha="right")
    beignet.show()


def ex1_1_2_camembert():
    beignet.pie(datablblbl['2020'], labels=datablblbl.Fonction)
    beignet.show()


def ex1_1_3_unification():
    datablblbl.plot(kind='bar', x='Fonction')  # TODO rien ça marche
    beignet.xticks(rotation=45, ha="right")
    beignet.show()


def main():
    ex1_1_print()
    ex1_1_1_baton()
    ex1_1_2_camembert()
    ex1_1_3_unification()
    return "panda is the impostor"


if __name__ == '__main__':
    main()
