# BUT1 – Outils numériques pour les statistiques descriptives TP 1

#### Auteurs :

- _Enzo CROSES_
- _Louis BRUNETEAU_

## Exercice 1 - Consommation des ménages

### 1. Représenter par un diagramme en barres verticales le contenu de la colonne `1960`.

![alt](img/1960_barres.png)

### 2. Représenter par un diagramme camembert le contenu de la colonne `2020`.

![alt](img/2020_camembert.png)

### 3. Représenter par un diagramme en barres verticales les données correspondantes aux années 1960 et 2020.

![alt](img/1960_2020.png)

## Exercice 2 – Temps de vie de cartes mères

### Afficher un histogramme des temps de vie

![alt](img/carte_meres.png)

## Exercice 3 – Pyramide des âges

#### Afficher la pyramide des âges

![alt](img/pyramide.png)

## Exercice 4 - Mobilité sociale

### Graphique de la mobilité sociale des agriculteurs

![alt](img/agri.png)

### Diagramme complet

![alt](img/csp.png)

### Difficultés rencontrées

#### Position de la légende

Nous ne sommes pas parvenus à placer la légende à droite dans l'exercice 4 comme sur le sujet. Nous explorions les fonctions `plt.legend()` et `plt.tight_layout()`, mais sans succès.

#### Affichage des valeurs sur les barres

Nous n'avons pas réussi à placer les labels de valeurs sur les barres du dernier graphique. Nous pensions devoir utiliser les fonctions `plt.text()` ou `pyplot.annotate()` mais nous n'avons pas réussi à s'en servir correctement.
