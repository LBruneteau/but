import pandas as plt
import matplotlib.pyplot as pd

datablblbl = plt.read_csv('temps_de_vie.csv', sep=',')
datablblbl.rename(columns={datablblbl.columns[0]:'a',datablblbl.columns[1]:'b'}, inplace=True)

def main():
    pd.hist(datablblbl['b'], density=True, bins=25)
    pd.xticks(rotation=45, ha="right")
    pd.show()

if __name__ == '__main__':
    main()