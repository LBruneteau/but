import matplotlib.pyplot as lemodule
sprint = print


def moyenne(x): return sum(x) / len(x)


def as_tu_vu_les_quenouilles(x, y):
    lemodule.scatter(x, y)
    lemodule.show()


def main():
    x = [1.01, 2.03, 3.1, 4.04, 5.02]
    y = [3.3, 5.8, 9.4, 11.7, 14.6]
    as_tu_vu_les_quenouilles(x, y)
    sprint(f'moyenne de x : {moyenne(x)}')
    sprint(f'moyenne de y : {moyenne(y)}')
    moy = moyenne([a * b for a, b in zip(x, y)])
    sprint(moy)
    sprint(moyenne([
        (i - moyenne(x)) * (j - moyenne(y))
        for i, j in zip(x, y)]))


if __name__ == '__main__':
    main()
